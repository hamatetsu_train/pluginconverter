﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using static System.Net.Mime.MediaTypeNames;
using System.Xml.Serialization;

namespace AtsPlugin
{
    public class Config : EngineBase
    {
        #region 定義
        public bool DebugMode;

        /// <summary>
        /// 車両設定
        /// </summary>
        public CFG_VEHICLE_SPEC VehicleSpec;

        public CFG_GENERAL General;

        /// <summary>
        /// DLLセクション
        /// </summary>
        public List<CFG_DLL> Dll;

        /// <summary>
        /// チェック対象の仮想キーコードリスト
        /// </summary>
        public List<int> KeyCodes;

        #region キーボードの入力方法
        public enum KeyInput_ENUM
        {
            AtsKey = 0,
            Keyboard = 1,
            Com = 2
        }
        #endregion
        
        #endregion

        #region クラス

        #region - Base
        public class CFG_BASE
        {

            public int GetIndexFromXml(XElement row, string ColumName, int _default)
            {
                if (row.Element(ColumName) != null && row.Element(ColumName).Value != null)
                {
                    if (int.TryParse(row.Element(ColumName).Value, out int i) == true)
                    {
                        if (i < 0 || i > 255)
                        {
                            return -1;
                        }
                        else
                        {
                            return i;
                        }
                    }
                    else
                    {
                        return _default;
                    }
                }
                else
                {
                    return _default;
                }

            }

            public int GetIndexFromXmlDirect(XElement row, int _default)
            {
                if (row != null && row.Value != null)
                {
                    if (int.TryParse(row.Value, out int i) == true)
                    {
                        if (i < 0 || i > 255)
                        {
                            return -1;
                        }
                        else
                        {
                            return i;
                        }
                    }
                    else
                    {
                        return _default;
                    }
                }
                else
                {
                    return _default;
                }

            }

            //protected void AddPanelIndexList(ref Dictionary<int, PanelIndex_ENUM> dt, int _index, PanelIndex_ENUM _subject)
            //{
            //    if (dt != null && _index != -1)
            //    {
            //        if (dt.ContainsKey(_index) == true)
            //        {
            //            dt[_index] = _subject;
            //        }
            //        else
            //        {
            //            dt.Add(_index, _subject);
            //        }
            //    }
            //}
            //protected void AddSoundIndexList(ref Dictionary<int, SoundIndex_ENUM> dt, int _index, SoundIndex_ENUM _subject)
            //{
            //    if (dt != null && _index != -1)
            //    {
            //        if (dt.ContainsKey(_index) == true)
            //        {
            //            dt[_index] = _subject;
            //        }
            //        else
            //        {
            //            dt.Add(_index, _subject);
            //        }
            //    }
            //}
        }

        #endregion

        #region VehicleSpec
        public class CFG_VEHICLE_SPEC
        {
            #region 変数定義
            /// <summary>
            /// 常用ブレーキノッチ数
            /// </summary>
            private int pSvcBrakeNotches;
            /// <summary>
            /// 常用ブレーキノッチ数
            /// </summary>
            public int SvcBrakeNotches
            {
                get { return this.pSvcBrakeNotches; }
            }

            /// <summary>
            /// 非常ブレーキノッチ段
            /// </summary>
            private int pEmgBrakeNotch;
            /// <summary>
            /// 非常ブレーキノッチ段
            /// </summary>
            public int EmgBrakeNotch
            {
                get { return this.pEmgBrakeNotch; }
            }

            /// <summary>
            /// 加速ブレーキノッチ数
            /// </summary>
            private int pPowerNotches;
            /// <summary>
            /// 加速ブレーキノッチ数
            /// </summary>
            public int PowerNotches
            {
                get { return this.pPowerNotches; }
            }

            /// <summary>
            /// ATSキャンセルノッチ
            /// </summary>
            private int pAtsCancelNotch;
            /// <summary>
            /// ATSキャンセルノッチ
            /// </summary>
            public int AtsCancelNotches
            {
                get { return this.pAtsCancelNotch; }
            }

            /// <summary>
            /// 車両数
            /// </summary>
            private int pCarCount;
            /// <summary>
            /// 車両数
            /// </summary>
            public int CarCount
            {
                get { return pCarCount; }
            }

            #endregion

            #region コンストラクタ
            public CFG_VEHICLE_SPEC()
            {
                this.pSvcBrakeNotches = 0;
                this.pEmgBrakeNotch = this.pSvcBrakeNotches + 1;
                this.pPowerNotches = 0;
                this.pAtsCancelNotch = 0;
                this.pCarCount = 0;
            }
            #endregion

            #region VehicleSpecのセット
            public void SetSpec(AtsDefine.AtsVehicleSpec vehicleSpec)
            {
                this.pSvcBrakeNotches = vehicleSpec.BrakeNotches;
                this.pEmgBrakeNotch = this.pSvcBrakeNotches + 1;
                this.pPowerNotches = vehicleSpec.PowerNotches;
                this.pAtsCancelNotch = vehicleSpec.AtsNotch;
                this.pCarCount = vehicleSpec.Cars;

            }
            #endregion
        }
        #endregion

        #region - General
        public class CFG_GENERAL : CFG_BASE
        {
            #region 定義

            /// <summary>
            /// ログレベル (0:なし 1:Info 2:Error 3:Debug)
            /// </summary>
            public int LogLevel { get; set; }

            private Dictionary<int, string[]> pBeaconScript;
            /// <summary>
            /// Beacon変換スクリプトのリスト(地上子番号, スクリプト)
            /// </summary>
            public Dictionary<int, string[]> BeaconScript
            {
                get { return this.pBeaconScript; }
                set { this.pBeaconScript = value; }
            }


            private Dictionary<int, string[]> pSignalScript;
            /// <summary>
            /// Signal変換スクリプトのリスト(信号インデックス, スクリプト)
            /// </summary>
            public Dictionary<int, string[]> SignalScript
            {
                get { return this.pSignalScript; }
                set { this.pSignalScript = value; }
            }
            #endregion

            #region コンストラクタ
            public CFG_GENERAL()
            {
                this.LogLevel = 1;
                this.pBeaconScript = new Dictionary<int, string[]>();
                this.pSignalScript = new Dictionary<int, string[]>();
            }
            #endregion

            #region セットする
            public void XmlSet(XElement row)
            {
                try
                {
                    var row2a = row.Elements("LogLevel");
                    if (row.Element("LogLevel") != null)
                    {
                        int level;
                        if (int.TryParse(row.Element("LogLevel").Value, out level) == true)
                        {
                            if (level < 0 || level > 3)
                            {
                                level = 1;
                            }
                        }
                        else
                        {
                            level = 1;
                        }
                        this.LogLevel = level;
                    }

                    // 配列なら…
                    var row2b = row.Elements("Beacon");
                    if (row2b != null)
                    {

                        foreach (XElement row3 in row2b)
                        {
                            try
                            {
                                // Scriptが配列
                                if (row3.Element("Type") != null && row3.Elements("Script") != null)
                                {
                                    int type;
                                    string script = "";
                                    if (int.TryParse(row3.Element("Type").Value, out type) == true)
                                    {
                                        if (type >= 0)
                                        {
                                            //script = row3.Element("Script").Value;
                                        }
                                        else
                                        {
                                            type = -1;
                                        }

                                    }
                                    else
                                    {
                                        type = -1;
                                    }

                                    if (type >= 0)
                                    {
                                        var row3b = row3.Elements("Script");
                                        foreach (XElement row4 in row3b)
                                        {
                                            script = row4.Value;
                                            if (this.pBeaconScript.ContainsKey(type) == true)
                                            {
                                                string[] strs = this.pBeaconScript[type];
                                                Array.Resize(ref strs, strs.Count() + 1);
                                                strs[strs.Count() - 1] = script;
                                                this.pBeaconScript[type] = strs;
                                            }
                                            else
                                            {
                                                string[] strs = { script };
                                                this.pBeaconScript.Add(type, strs);
                                            }
                                        }
                                    }
                                }
                            }
                            catch
                            {
                                // Scriptが単体
                                if (row3.Element("Type") != null && row3.Element("Script") != null)
                                {
                                    int type;
                                    string script = "";
                                    if (int.TryParse(row3.Element("Type").Value, out type) == true)
                                    {
                                        if (type >= 0)
                                        {
                                            script = row3.Element("Script").Value;
                                        }
                                        else
                                        {
                                            type = -1;
                                        }

                                    }
                                    else
                                    {
                                        type = -1;
                                    }

                                    if (type >= 0)
                                    {
                                        if (this.pBeaconScript.ContainsKey(type) == true)
                                        {
                                            string[] strs = this.pBeaconScript[type];
                                            Array.Resize(ref strs, strs.Count() + 1);
                                            strs[strs.Count() - 1] = script;
                                            this.pBeaconScript[type] = strs;
                                        }
                                        else
                                        {
                                            string[] strs = { script };
                                            this.pBeaconScript.Add(type, strs);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch
                {
                    // 単体
                    var row2c = row.Element("Beacon");
                    if (row2c != null)
                    {
                        try
                        {
                            if (row2c.Element("Type") != null && row2c.Elements("Script") != null)
                            {
                                int type;
                                string script = "";
                                if (int.TryParse(row2c.Element("Type").Value, out type) == true)
                                {
                                    if (type >= 0)
                                    {
                                        //script = row2c.Element("Script").Value;
                                    }
                                    else
                                    {
                                        type = -1;
                                    }

                                }
                                else
                                {
                                    type = -1;
                                }

                                if (type >= 0)
                                {
                                    var row3c = row2c.Elements("Script");
                                    foreach (XElement row4 in row3c)
                                    {
                                        script = row4.Value;
                                        if (this.pBeaconScript.ContainsKey(type) == true)
                                        {
                                            string[] strs = this.pBeaconScript[type];
                                            Array.Resize(ref strs, strs.Count() + 1);
                                            strs[strs.Count() - 1] = script;
                                            this.pBeaconScript[type] = strs;
                                        }
                                        else
                                        {
                                            string[] strs = { script };
                                            this.pBeaconScript.Add(type, strs);
                                        }
                                    }
                                }
                            }
                        }
                        catch
                        {
                            if (row2c.Element("Type") != null && row2c.Element("Script") != null)
                            {
                                int type;
                                string script = "";
                                if (int.TryParse(row2c.Element("Type").Value, out type) == true)
                                {
                                    if (type >= 0)
                                    {
                                        script = row2c.Element("Script").Value;
                                    }
                                    else
                                    {
                                        type = -1;
                                    }

                                }
                                else
                                {
                                    type = -1;
                                }

                                if (type >= 0)
                                {
                                    if (this.pBeaconScript.ContainsKey(type) == true)
                                    {
                                        string[] strs = this.pBeaconScript[type];
                                        Array.Resize(ref strs, strs.Count() + 1);
                                        strs[strs.Count() - 1] = script;
                                        this.pBeaconScript[type] = strs;
                                    }
                                    else
                                    {
                                        string[] strs = { script };
                                        this.pBeaconScript.Add(type, strs);
                                    }
                                }
                            }
                        }
                    }
                }

                try
                {
                    // 配列なら…
                    var row2b = row.Elements("Signal");
                    if (row2b != null)
                    {

                        foreach (XElement row3 in row2b)
                        {
                            try
                            {
                                // Scriptが配列
                                if (row3.Element("Index") != null && row3.Elements("Script") != null)
                                {
                                    int type;
                                    string script = "";
                                    if (int.TryParse(row3.Element("Index").Value, out type) == true)
                                    {
                                        if (type >= 0)
                                        {
                                            //script = row3.Element("Script").Value;
                                        }
                                        else
                                        {
                                            type = -1;
                                        }

                                    }
                                    else
                                    {
                                        type = -1;
                                    }

                                    if (type >= 0)
                                    {
                                        var row3b = row3.Elements("Script");
                                        foreach (XElement row4 in row3b)
                                        {
                                            script = row4.Value;
                                            if (this.pSignalScript.ContainsKey(type) == true)
                                            {
                                                string[] strs = this.pSignalScript[type];
                                                Array.Resize(ref strs, strs.Count() + 1);
                                                strs[strs.Count() - 1] = script;
                                                this.pSignalScript[type] = strs;
                                            }
                                            else
                                            {
                                                string[] strs = { script };
                                                this.pSignalScript.Add(type, strs);
                                            }
                                        }
                                    }
                                }
                            }
                            catch
                            {
                                // Scriptが単体
                                if (row3.Element("Index") != null && row3.Element("Script") != null)
                                {
                                    int type;
                                    string script = "";
                                    if (int.TryParse(row3.Element("Index").Value, out type) == true)
                                    {
                                        if (type >= 0)
                                        {
                                            script = row3.Element("Script").Value;
                                        }
                                        else
                                        {
                                            type = -1;
                                        }

                                    }
                                    else
                                    {
                                        type = -1;
                                    }

                                    if (type >= 0)
                                    {
                                        if (this.pSignalScript.ContainsKey(type) == true)
                                        {
                                            string[] strs = this.pSignalScript[type];
                                            Array.Resize(ref strs, strs.Count() + 1);
                                            strs[strs.Count() - 1] = script;
                                            this.pSignalScript[type] = strs;
                                        }
                                        else
                                        {
                                            string[] strs = { script };
                                            this.pSignalScript.Add(type, strs);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch
                {
                    // 単体
                    var row2c = row.Element("Signal");
                    if (row2c != null)
                    {
                        try
                        {
                            if (row2c.Element("Index") != null && row2c.Elements("Script") != null)
                            {
                                int type;
                                string script = "";
                                if (int.TryParse(row2c.Element("Index").Value, out type) == true)
                                {
                                    if (type >= 0)
                                    {
                                        //script = row2c.Element("Script").Value;
                                    }
                                    else
                                    {
                                        type = -1;
                                    }

                                }
                                else
                                {
                                    type = -1;
                                }

                                if (type >= 0)
                                {
                                    var row3c = row2c.Elements("Script");
                                    foreach (XElement row4 in row3c)
                                    {
                                        script = row4.Value;
                                        if (this.pSignalScript.ContainsKey(type) == true)
                                        {
                                            string[] strs = this.pSignalScript[type];
                                            Array.Resize(ref strs, strs.Count() + 1);
                                            strs[strs.Count() - 1] = script;
                                            this.pSignalScript[type] = strs;
                                        }
                                        else
                                        {
                                            string[] strs = { script };
                                            this.pSignalScript.Add(type, strs);
                                        }
                                    }
                                }
                            }
                        }
                        catch
                        {
                            if (row2c.Element("Index") != null && row2c.Element("Script") != null)
                            {
                                int type;
                                string script = "";
                                if (int.TryParse(row2c.Element("Index").Value, out type) == true)
                                {
                                    if (type >= 0)
                                    {
                                        script = row2c.Element("Script").Value;
                                    }
                                    else
                                    {
                                        type = -1;
                                    }

                                }
                                else
                                {
                                    type = -1;
                                }

                                if (type >= 0)
                                {
                                    if (this.pSignalScript.ContainsKey(type) == true)
                                    {
                                        string[] strs = this.pSignalScript[type];
                                        Array.Resize(ref strs, strs.Count() + 1);
                                        strs[strs.Count() - 1] = script;
                                        this.pSignalScript[type] = strs;
                                    }
                                    else
                                    {
                                        string[] strs = { script };
                                        this.pSignalScript.Add(type, strs);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            #region 作成
            public XElement MakeXml()
            {
                try
                {
                    XElement xml = new XElement("DLL");
                    XElement data;
                    XElement data2;
                    //XElement data3;

                    data = new XElement("LogLevel", this.LogLevel);
                    xml.Add(data);

                    if (this.pBeaconScript.Count > 0)
                    {
                        data = new XElement("Beacon");
                        foreach (KeyValuePair<int, string[]> kvp in this.pBeaconScript)
                        {
                            data2 = new XElement("Type", kvp.Key);
                            data.Add(data2);
                            foreach(string str in kvp.Value)
                            {
                                data2 = new XElement("Script", str);
                                data.Add(data2);
                            }
                            xml.Add(data);
                        }
                    }

                    if (this.pBeaconScript.Count > 0)
                    {
                        data = new XElement("Signal");
                        foreach (KeyValuePair<int, string[]> kvp in this.pSignalScript)
                        {
                            data2 = new XElement("Index", kvp.Key);
                            data.Add(data2);
                            foreach (string str in kvp.Value)
                            {
                                data2 = new XElement("Script", str);
                                data.Add(data2);
                            }
                            xml.Add(data);
                        }
                    }

                    return xml;
                }
                catch (Exception ex)
                {
                    ExceptionMessage(ex, "make dll", true);
                    return null;
                }
            }
            #endregion

        }
        #endregion

        #region - Dll
        public class CFG_DLL : CFG_BASE
        {
            #region 定義
            public string OldDllName { get; set; }
            public string OldDllDirectory { get; set; }

            /// <summary>
            /// DLLのパス(メインからの相対パス)
            /// </summary>
            public string DllPath { get; set; }

            private bool pHasSetHandle;
            /// <summary>
            /// SetHandle関数を持っているか？
            /// </summary>
            public bool HasSetHandle
            {
                get { return this.pHasSetHandle; }
                set { this.pHasSetHandle = value; }
            }

            private bool pHasGetVehicleState;
            /// <summary>
            /// GetVehicleState関数を持っているか？
            /// </summary>
            public bool HasGetVehicleState
            {
                get { return this.pHasGetVehicleState; }
                set { this.pHasGetVehicleState = value; }
            }

            private bool pInitializeNfbOn;
            /// <summary>
            /// 初期動作状態
            /// </summary>
            public bool InitializeNfbOn
            {
                get { return this.pInitializeNfbOn; }
                set { this.pInitializeNfbOn = value; }
            }

            private bool pGetHandle;
            /// <summary>
            /// 前のDLLからハンドル値をもらうか？
            /// </summary>
            public bool GetHandle
            {
                get { return this.pGetHandle; }
                set { this.pGetHandle = value; }
            }

            private bool pGetPower;
            /// <summary>
            /// 前のDLLからPowerハンドル値をもらうか？
            /// </summary>
            public bool GetPower
            {
                get { return this.pGetPower; }
                set { this.pGetPower = value; }
            }

            private bool pGetBrake;
            /// <summary>
            /// 前のDLLからBrakeハンドル値をもらうか？
            /// </summary>
            public bool GetBrake
            {
                get { return this.pGetBrake; }
                set { this.pGetBrake = value; }
            }

            private bool pGetReverser;
            /// <summary>
            /// 前のDLLからReverserハンドル値をもらうか？
            /// </summary>
            public bool GetReverser
            {
                get { return this.pGetReverser; }
                set { this.pGetReverser = value; }
            }

            private bool pSetPower;
            /// <summary>
            /// 以降のDLLにPowerハンドル値を渡すか？
            /// </summary>
            public bool SetPower
            {
                get { return this.pSetPower; }
                set { this.pSetPower = value; }
            }

            private bool pSetBrake;
            /// <summary>
            /// 以降のDLLにBrakeハンドル値を渡すか？
            /// </summary>
            public bool SetBrake
            {
                get { return this.pSetBrake; }
                set { this.pSetBrake = value; }
            }

            private bool pSetReverser;
            /// <summary>
            /// 以降のDLLにReverserハンドル値を渡すか？
            /// </summary>
            public bool SetReverser
            {
                get { return this.pSetReverser; }
                set { this.pSetReverser = value; }
            }

            private bool pSetCsc;
            /// <summary>
            /// 以降のDLLにCscハンドル値を渡すか？
            /// </summary>
            public bool SetCsc
            {
                get { return this.pSetCsc; }
                set { this.pSetCsc = value; }
            }

            public List<KeyControl> keyControl { get; set; }

            #endregion

            public class KeyControl
            {
                #region 変数定義
                /// <summary>
                /// キーの入力方法
                /// </summary>
                private Config.KeyInput_ENUM pKeyInput;
                /// <summary>
                /// キーの入力方法
                /// </summary>
                public Config.KeyInput_ENUM KeyInput
                {
                    get { return this.pKeyInput; }
                    set { this.pKeyInput = value; }
                }

                /// <summary>
                /// キー番号
                /// </summary>
                private int pKeyCode;
                /// <summary>
                /// キー番号
                /// </summary>
                public int KeyCode
                {
                    get { return this.pKeyCode; }
                    set { this.pKeyCode = value; }
                }

                private bool pIsExclusive;
                /// <summary>
                /// 排他でONにする
                /// </summary>
                public bool IsExclusive
                {
                    get { return this.pIsExclusive; }
                    set { this.pIsExclusive = value; }
                }

                #endregion

                #region コンストラクタ
                public KeyControl()
                {
                    this.pKeyInput = KeyInput_ENUM.Keyboard;
                    this.pKeyCode = 0;
                    this.pIsExclusive = false;
                }
                #endregion

                #region セットする
                public void XmlSet(XElement row, string Mode)
                {
                    if (row.Element("Input") != null && row.Element("Input").Value != null)
                    {
                        if (Enum.TryParse<KeyInput_ENUM>(row.Element("Input").Value, out this.pKeyInput) == false)
                        {
                            this.pKeyInput = KeyInput_ENUM.Keyboard;
                        }
                    }

                    if (row.Element("KeyCode") != null && row.Element("KeyCode").Value != null)
                    {
                        if (this.pKeyInput == KeyInput_ENUM.AtsKey)  //if (this.pIsAtsKey == true)
                        {
                            // ATSキーの場合
                            if (int.TryParse(row.Element("KeyCode").Value, out this.pKeyCode) == true)
                            {
                                // ATSキーはS(0)～L(15)
                                if (this.pKeyCode < 0 || this.pKeyCode > 15)
                                {
                                    this.pKeyCode = -1;
                                    Log.Error("キーアサイン 不明なATSキーが指定されています : " + row.Element("KeyCode").Value);
                                }
                            }
                            else
                            {
                                this.pKeyCode = -1;
                                Log.Error("キーアサイン 不明なATSキーが指定されています : " + row.Element("KeyCode").Value);
                            }
                        }
                        else
                        {
                            // 仮想キーコード・外部入力の場合
                            if (int.TryParse(row.Element("KeyCode").Value, out this.pKeyCode) == true)
                            {
                                // 仮想キーコードは1～254 0は使用しない
                                if ((this.pKeyInput == KeyInput_ENUM.Keyboard && (this.pKeyCode <= 0 || this.pKeyCode >= 255)) ||
                                    (this.pKeyInput == KeyInput_ENUM.Com && (this.pKeyCode <= 0 || this.pKeyCode >= 65535)))
                                {
                                    this.pKeyCode = -1;
                                    Log.Error("キーアサイン 不明なキーコードが指定されています : " + row.Element("KeyCode").Value);
                                }
                                else
                                {
#if DEBUG
                                    //MessageBox.Show("キーアサイン\n[" + Mode + "]にキーコードを設定しました。", "設定読み込み");
#endif
                                }
                            }
                            else
                            {
                                this.pKeyCode = -1;
                                Log.Error("キーアサイン 不明なキーコードが指定されています : " + row.Element("KeyCode").Value);
                            }
                        }
                    }

                    if (row.Element("IsExclusive") != null && row.Element("IsExclusive").Value != null)
                    {
                        if (bool.TryParse(row.Element("IsExclusive").Value, out this.pIsExclusive) == false)
                        {
                            this.IsExclusive = false;
                        }
                    }
                }

                #endregion

                #region 作成
                public XElement MakeXml()
                {
                    try
                    {
                        if ((this.KeyInput == KeyInput_ENUM.AtsKey && this.KeyCode == -1) ||
                            (this.KeyInput != KeyInput_ENUM.AtsKey && this.KeyCode == 0))
                        {
                            return null;
                        }
                        else
                        {
                            XElement xml = new XElement("KeyAssign");
                            XElement data;

                            data = new XElement("Input", this.KeyInput.ToString());
                            xml.Add(data);

                            data = new XElement("KeyCode", this.KeyCode);
                            xml.Add(data);

                            data = new XElement("IsExclusive", this.IsExclusive);
                            xml.Add(data);

                            return xml;
                        }
                    }
                    catch (Exception ex)
                    {
                        ExceptionMessage(ex, "make keycontrol", true);
                        return null;
                    }
                }
#endregion
            }

            #region コンストラクタ
            public CFG_DLL()
            {
                this.OldDllName = "";
                this.OldDllDirectory = "";
                this.DllPath = "";
                this.pHasSetHandle = false;
                this.pHasGetVehicleState = false;
                this.pInitializeNfbOn = true;
                this.keyControl = new List<KeyControl>();
                this.pGetHandle = false;
                this.pGetPower = true;
                this.pGetBrake = true;
                this.pGetReverser = true;
                this.pSetPower = true;
                this.pSetBrake = true;
                this.pSetReverser = true;
                this.pSetCsc = true;
            }
            #endregion

            #region セットする
            public void XmlSet(XElement row)
            {
                if (row.Element("Path") != null && row.Element("Path").Value != null)
                {
                    try
                    {
                        this.OldDllName = System.IO.Path.GetFileName(row.Element("Path").Value);
                    }
                    catch
                    {
                        this.OldDllName = row.Element("Path").Value;
                    }
                    this.OldDllDirectory = row.Element("Path").Value;

                    if (System.IO.File.Exists(ModuleDirectoryPath + System.IO.Path.DirectorySeparatorChar + row.Element("Path").Value) == true)
                    {
                        this.DllPath = ModuleDirectoryPath + System.IO.Path.DirectorySeparatorChar + row.Element("Path").Value;
                    }
                    else
                    {
#if DEBUG
                        Log.Write("不明なパス : " + ModuleDirectoryPath + System.IO.Path.DirectorySeparatorChar + row.Element("Path").Value);
#endif
                    }
                }

                if (row.Element("HasSetHandle") != null && row.Element("HasSetHandle").Value != null)
                {
                    if (bool.TryParse(row.Element("HasSetHandle").Value, out this.pHasSetHandle) == false)
                    {
                        this.pHasSetHandle = false;
                    }
                }

                if (row.Element("HasGetVehicleState") != null && row.Element("HasGetVehicleState").Value != null)
                {
                    if (bool.TryParse(row.Element("HasGetVehicleState").Value, out this.pHasGetVehicleState) == false)
                    {
                        this.pHasGetVehicleState = false;
                    }
                }

                if (row.Element("InitializeNfbOn") != null && row.Element("InitializeNfbOn").Value != null)
                {
                    if (bool.TryParse(row.Element("InitializeNfbOn").Value, out this.pInitializeNfbOn) == false)
                    {
                        this.pInitializeNfbOn = true;
                    }
                }

                var row2 = row.Element("Get");
                if (row2 != null)
                {
                    if (row2.Element("Handle") != null && row2.Element("Handle").Value != null)
                    {
                        if (bool.TryParse(row2.Element("Handle").Value, out this.pGetHandle) == false)
                        {
                            this.pGetHandle = false;
                        }
                    }

                    if (row2.Element("Power") != null && row2.Element("Power").Value != null)
                    {
                        if (bool.TryParse(row2.Element("Power").Value, out this.pGetPower) == false)
                        {
                            this.pGetPower = false;
                        }
                    }

                    if (row2.Element("Brake") != null && row2.Element("Brake").Value != null)
                    {
                        if (bool.TryParse(row2.Element("Brake").Value, out this.pGetBrake) == false)
                        {
                            this.pGetBrake = false;
                        }
                    }

                    if (row2.Element("Reverser") != null && row2.Element("Reverser").Value != null)
                    {
                        if (bool.TryParse(row2.Element("Reverser").Value, out this.pGetReverser) == false)
                        {
                            this.pGetReverser = false;
                        }
                    }

                }

                row2 = row.Element("Set");
                if (row2 != null)
                {
                    if (row2.Element("Power") != null && row2.Element("Power").Value != null)
                    {
                        if (bool.TryParse(row2.Element("Power").Value, out this.pSetPower) == false)
                        {
                            this.pSetPower = true;
                        }
                    }

                    if (row2.Element("Brake") != null && row2.Element("Brake").Value != null)
                    {
                        if (bool.TryParse(row2.Element("Brake").Value, out this.pSetBrake) == false)
                        {
                            this.pSetBrake = true;
                        }
                    }

                    if (row2.Element("Reverser") != null && row2.Element("Reverser").Value != null)
                    {
                        if (bool.TryParse(row2.Element("Reverser").Value, out this.pSetReverser) == false)
                        {
                            this.pSetReverser = true;
                        }
                    }

                    if (row2.Element("Csc") != null && row2.Element("Csc").Value != null)
                    {
                        if (bool.TryParse(row2.Element("Csc").Value, out this.pSetCsc) == false)
                        {
                            this.pSetCsc = true;
                        }
                    }
                }

                var table = row.Elements("KeyAssign");
                if (table != null)
                {
                    foreach (XElement row3 in table)
                    {
                        KeyControl onekey = new KeyControl();
                        onekey.XmlSet(row3, "");
                        if (onekey.KeyCode != -1)
                        {
                            this.keyControl.Add(onekey);
                        } 
                    }
                }


                //XElement row2 = row.Element("PowerHandleIndex");
                //if (row2 != null)
                //{
                //    this.pInPowerHandleIndex = GetIndexFromXml(row2, "Input", this.pInPowerHandleIndex);
                //    this.pOutPowerHandleIndex = GetIndexFromXml(row2, "Output", this.pOutPowerHandleIndex);
                //    this.pPanelPowerHandleIndex = GetIndexFromXml(row2, "Panel", this.pPanelPowerHandleIndex);
                //}

                //row2 = row.Element("BrakeHandleIndex");
                //if (row2 != null)
                //{
                //    this.pInBrakeHandleIndex = GetIndexFromXml(row2, "Input", this.pInBrakeHandleIndex);
                //    this.pOutBrakeHandleIndex = GetIndexFromXml(row2, "Output", this.pOutBrakeHandleIndex);
                //    this.pPanelBrakeHandleIndex = GetIndexFromXml(row2, "Panel", this.pPanelBrakeHandleIndex);
                //}

                //row2 = row.Element("ReverserHandleIndex");
                //if (row2 != null)
                //{
                //    this.pInReverserHandleIndex = GetIndexFromXml(row2, "Input", this.pInReverserHandleIndex);
                //    this.pOutReverserHandleIndex = GetIndexFromXml(row2, "Output", this.pOutReverserHandleIndex);
                //    this.pPanelReverserHandleIndex = GetIndexFromXml(row2, "Panel", this.pPanelReverserHandleIndex);
                //}

                //row2 = row.Element("CscValueIndex");
                //if (row2 != null)
                //{
                //    this.pInCscValueIndex = GetIndexFromXml(row2, "Input", this.pInCscValueIndex);
                //    this.pOutCscValueIndex = GetIndexFromXml(row2, "Output", this.pOutCscValueIndex);
                //}

            }
            #endregion

            #region 作成
            public XElement MakeXml()
            {
                try
                {
                    XElement xml = new XElement("DLL");
                    XElement data;
                    XElement data2;
                    //XElement data3;

                    data = new XElement("Path", this.OldDllDirectory);
                    xml.Add(data);

                    if (this.HasSetHandle == true)
                    {
                        data = new XElement("HasSetHandle", this.HasSetHandle);
                        xml.Add(data);
                    }

                    if (this.HasGetVehicleState == true)
                    {
                        data = new XElement("HasGetVehicleState", this.HasGetVehicleState);
                        xml.Add(data);
                    }

                    if (this.InitializeNfbOn == false)
                    {
                        data = new XElement("InitializeNfbOn", this.InitializeNfbOn);
                        xml.Add(data);
                    }

                    if (this.HasSetHandle == true && this.GetHandle == false)
                    {
                        data = new XElement("Get");
                        data2 = new XElement("Handle", this.GetHandle);
                        data.Add(data2);
                        xml.Add(data);
                    }
                    else if(this.HasSetHandle == false && (this.GetPower == false || this.GetBrake == false || this.GetReverser == false))
                    {
                        data = new XElement("Get");

                        if (this.GetPower == false)
                        {
                            data2 = new XElement("Power", this.GetPower);
                            data.Add(data2);
                        }

                        if (this.GetBrake == false)
                        {
                            data2 = new XElement("Brake", this.GetBrake);
                            data.Add(data2);
                        }

                        if (this.GetReverser == false)
                        {
                            data2 = new XElement("Reverser", this.GetReverser);
                            data.Add(data2);
                        }

                        xml.Add(data);
                    }

                    if (this.SetPower == false || this.SetBrake == false || this.SetReverser == false || this.SetCsc == false)
                    {
                        data = new XElement("Set");

                        if (this.SetPower == false)
                        {
                            data2 = new XElement("Power", this.SetPower);
                            data.Add(data2);
                        }

                        if (this.SetBrake == false)
                        {
                            data2 = new XElement("Brake", this.SetBrake);
                            data.Add(data2);
                        }

                        if (this.SetReverser == false)
                        {
                            data2 = new XElement("Reverser", this.SetReverser);
                            data.Add(data2);
                        }

                        if (this.SetCsc == false)
                        {
                            data2 = new XElement("Csc", this.SetCsc);
                            data.Add(data2);
                        }

                        xml.Add(data);
                    }

                    foreach(KeyControl keyControl in this.keyControl)
                    {
                        data = keyControl.MakeXml();
                        if (data != null)
                        {
                            xml.Add(data);
                        }
                    }

                    return xml;
                }
                catch (Exception ex)
                {
                    ExceptionMessage(ex, "make dll", true);
                    return null;
                }
            }
#endregion
        }

        #endregion

        #endregion

        #region コンストラクタ
        public Config()
        {
            this.DebugMode = false;
            this.VehicleSpec = new CFG_VEHICLE_SPEC();
            this.General = new CFG_GENERAL();
            this.Dll = new List<CFG_DLL>();
            this.KeyCodes = new List<int>();
        }
        #endregion

        #region 読み込み
        public bool MainLoad()
        {
            try
            {
                if (System.IO.Path.GetExtension(ModulePathWithFileName) != "")
                {
#if DEBUG
                    Log.Write("[Config.MainLoad] : 自身のファイルパス = " + ModulePathWithFileName);
#endif
                    string XmlPath = ModulePathWithFileName.Replace(System.IO.Path.GetExtension(ModulePathWithFileName), ".xml"); // 拡張子を.xmlに変更
                    return Load(XmlPath);
                }
                else
                {
                    Log.Write("[Config.MainLoad] : 自身のファイルパスの拡張子取得に失敗？ : " + ModulePathWithFileName);
                    return false;
                }
            }
            catch (Exception ex)
            {
                Log.Write("[Config.MainLoad] : 設定ファイルの読み込み中にエラー : " + ex.Message);
                return false;
            }
        }

        public bool Load(string XmlPath)
        {
            try
            {
                if (System.IO.File.Exists(XmlPath) == true)
                {
/*
                    var xs = new XmlSerializer(typeof(Config));
                    Config cfg = null;
                    using (var sr = new StreamReader(XmlPath, Encoding.UTF8))
                    using (var xr = System.Xml.XmlReader.Create(sr))
                    {
                        cfg = (Config)xs.Deserialize(xr); // Configクラスにキャストする
                    }
                    if(cfg != null)
                    {
                        this.General = cfg.General;
                        this.Dll = cfg.Dll;
                    }
*/                    

                    var table = XDocument.Load(XmlPath).Element("PluginConverter");

                    // GENERALセクション
                    var row = table.Element("GENERAL");
                    if (row != null)
                    {
                        this.General.XmlSet(row);
                    }

                    // DLLセクション(配列)
                    var row2 = table.Elements("DLL");
                    if (row2 != null)
                    {
                        foreach (XElement row3 in row2)
                        {
                            CFG_DLL onedll = new CFG_DLL();
                            onedll.XmlSet(row3);
                            if (onedll.DllPath != "" || (this.DebugMode == true && onedll.OldDllDirectory != ""))
                            {
                                this.Dll.Add(onedll);
                                // キーコードリスト追加
                                if (onedll.keyControl != null && onedll.keyControl.Count > 0)
                                {
                                    foreach (CFG_DLL.KeyControl KC in onedll.keyControl)
                                    {
                                        if (KC.KeyInput == KeyInput_ENUM.Keyboard)
                                        {
                                            AddKeyCode(KC.KeyCode);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    return true;
                }
                else
                {
                    Log.Write("[Config.Load] : 設定ファイルが見つからない。欲しかったファイルパス=" + XmlPath);
                    return false;
                }
            }
            catch (Exception ex)
            {
                Log.Write("[Config.Load] : 設定ファイルの読み込み中にエラー : " + ex.Message);
                return false;
            }
        }

        public bool Save(string filepath)
        {
            try
            {
                XDocument xdoc = new XDocument(new XDeclaration("1.0", "Shift-JIS", "true"));
                XElement xml = new XElement("PluginConverter");

                XElement data = General.MakeXml();
                if (data != null)
                {
                    xml.Add(data);
                }

                if (this.Dll != null)
                {
                    foreach(CFG_DLL dll in this.Dll)
                    {
                        data = dll.MakeXml();
                        if (data != null)
                        {
                            xml.Add(data);
                        }
                    }
                }

                xdoc.Add(xml);
                xdoc.Save(filepath);

                return true;
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "Save", true);
                return false;
            }
        }
        #endregion

        #region キーコードリスト
        private void AddKeyCode(int pKeyCode)
        {
            try
            {
                if (this.KeyCodes == null)
                {
                    this.KeyCodes = new List<int>();
                }

                if (pKeyCode > 0 && this.KeyCodes.Contains(pKeyCode) == false)
                {
                    this.KeyCodes.Add(pKeyCode);
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "", true);
            }
        }
        #endregion

    }
}
