﻿using System;
//using System.Collections.Generic;
using System.IO;
//using System.Linq;
using System.Reflection;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace AtsPlugin
{
    public class EngineBase
    {
        #region static

        public static string PluginName = "変換プラグイン";

        #region - DLLのパス
        private static string pModulePathWithFileName = "";
        public static string ModulePathWithFileName
        {
            get
            {
                if (pModulePathWithFileName == "")
                {
                    pModulePathWithFileName = Assembly.GetExecutingAssembly().Location;
                }
                return pModulePathWithFileName;
            }
        }
        #endregion

        #region - DLLのファイル名
        private static string pModuleName = "";
        public static string ModuleName
        {
            get
            {
                if (pModuleName == "")
                {
                    pModuleName = Path.GetFileName(ModulePathWithFileName);
                }
                return pModuleName;
            }
        }
        #endregion

        #region - DLLのフォルダ
        private static string pModuleDirectoryPath = "";
        public static string ModuleDirectoryPath
        {
            get
            {
                if (pModuleDirectoryPath == "")
                {
                    pModuleDirectoryPath = Path.GetDirectoryName(ModulePathWithFileName); ;
                }
                return pModuleDirectoryPath;
            }
        }
        #endregion

        #region - DLLのバージョン
        private static string pModuleVersion = "";
        public static string ModuleVersion
        {
            get
            {
                if (pModuleVersion == "")
                {
                    pModuleVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                }
                return pModuleVersion;
            }
        }
        #endregion

        #region - ログ
        public static class Log
        {
            private static string FilePath = "";
            private static FileStream fs = null;
            private static bool fserror = false;
            private static int level = 2; // 0:なし 1:Info 2: Error 3: Debug

            public static void Init()
            {
                try
                {
                    // 使われてたらそのまま使う
                    if (fs == null)
                    {
                        // ログファイルパスの生成
                        FilePath = ModulePathWithFileName.Remove(ModulePathWithFileName.Length - 4, 4) + ".log";
                        // ファイルを空にしたい
                        if (File.Exists(FilePath) == true)
                        {
                            File.Delete(FilePath);
                        }
                        // オープン
                        fs = File.Open(FilePath, FileMode.OpenOrCreate);
                        fs.Seek(0, SeekOrigin.End);
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("ログファイルオープン処理でエラーが発生しました。\n" + ex.Message, PluginName);
                }
            }
            public static void SetLevel(int _level)
            {
                try
                {
                    level = _level;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ログレベル設定処理でエラーが発生しました。\n" + ex.Message, PluginName);
                }
            }
            public static void Dispose()
            {
                try
                {
                    if (fs != null)
                    {
                        fs.Dispose();
                        //fs = null;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ログファイルクローズ処理でエラーが発生しました。\n" + ex.Message, PluginName);
                }
            }

            public static void Write(string message)
            {
                try
                {
                    if (fs != null)
                    {
                        UTF8Encoding utf8 = new UTF8Encoding();
                        byte[] result = utf8.GetBytes(DateTime.Now.ToString("HH:mm:ss.fff") + ":" + message + "\n");
                        fs.WriteAsync(result, 0, result.Length);
                    }
                }
                catch
                {
                    try
                    {
                        if (fs != null && fserror == false)
                        {
                            // 一度生成を試す
                            // ログファイルパスの生成
                            FilePath = ModulePathWithFileName.Remove(ModulePathWithFileName.Length - 4, 4) + ".log";
                            // オープン
                            fs = File.Open(FilePath, FileMode.OpenOrCreate);
                            fs.Seek(0, SeekOrigin.End);
                        }
                    }
                    catch (Exception ex2)
                    {
                        MessageBox.Show("ログ処理でエラーが発生しました。\n" + ex2.Message, PluginName);
                        fserror = true;
                    }
                }
            }

            /// <summary>
            /// 通常メッセージ
            /// </summary>
            /// <param name="message">メッセージ</param>
            public static void Info(string message)
            {
                try
                {
                    if (level >= 1)
                    {
                        Write("[Info] " + message);
                    }
#if DEBUG
                    else 
                    {
                        Write("[Info(NG"+level.ToString()+")] " + message);
                    }
#endif
                }
                catch (Exception ex2)
                {
                    MessageBox.Show("ログ処理でエラーが発生しました。\n" + ex2.Message, PluginName);
                }
            }

            /// <summary>
            /// エラーメッセージ
            /// </summary>
            /// <param name="message">ログ</param>
            /// <param name="ex">例外(スタックトレースを出力したいとき)</param>
            public static void Error(string message, Exception ex = null)
            {
                try
                {
                    if (level >= 2)
                    {
                        if(ex == null)
                        {
                            Write("[Error] " + message);
                        }
                        else
                        {
                            Write("[Error] " + message + "\n" + ex.StackTrace);
                        }
                    }
#if DEBUG
                    else
                    {
                        Write("[Error(NG"+level.ToString()+")] " + message);
                    }
#endif
                }
                catch (Exception ex2)
                {
                    MessageBox.Show("ログ処理でエラーが発生しました。\n" + ex2.Message, PluginName);
                }
            }

            /// <summary>
            /// デバッグメッセージ
            /// </summary>
            /// <param name="message">ログ</param>
            /// <param name="ex">例外(スタックトレースを出力したいとき)</param>
            public static void Debug(string message, Exception ex = null)
            {
                try
                {
                    if (level >= 3)
                    {
                        if (ex == null)
                        {
                            Write("[Debug] " + message);
                        }
                        else
                        {
                            Write("[Debug] " + message + "\n" + ex.StackTrace);
                        }
                    }
#if DEBUG
                    else
                    {
                        Write("[Debug(NG" + level.ToString() + ")] " + message);
                    }
#endif
                }
                catch (Exception ex2)
                {
                    MessageBox.Show("ログ処理でエラーが発生しました。\n" + ex2.Message, PluginName);
                }
            }
        }

        #endregion

        #endregion

        #region エラー表示
        /// <summary>
        /// エラー表示回数
        /// </summary>
        private int ExceptionCounter = 0;

        /// <summary>
        /// エラーダイアログを表示する
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="addtext"></param>
        /// <param name="showDialog"></param>
        public static void ExceptionMessage(Exception ex, string addtext, bool showDialog)
        {
#if DEBUG
            MessageBox.Show(addtext + "\n" + ex.Message, EngineBase.PluginName);
#else
            Log.Error(addtext + " " + ex.Message, ex);
            MessageBox.Show(addtext + "\n" + ex.Message, EngineBase.PluginName);
#endif
        }

        /// <summary>
        /// １回目だけエラーダイアログを表示する
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="addtext"></param>
        /// <param name="showDialog"></param>
        public void ExceptionMessageOnce(Exception ex, string addtext, bool showDialog)
        {
#if DEBUG
            if (ExceptionCounter == 0)
            {
                ExceptionCounter++;
                MessageBox.Show(addtext + "\n" + ex.Message, EngineBase.PluginName);
            }
            else
            {
                Console.Write(addtext + "\n" + ex.Message);
            }
#else
            if (showDialog == true && ExceptionCounter == 0)
            {
                ExceptionCounter++;
                MessageBox.Show(addtext + "\n" + ex.Message, EngineBase.PluginName);
            }
            else
            {
                Console.Write(addtext + "\n" + ex.Message);
            }
            Log.Error(addtext + " " + ex.Message,ex);
#endif
        }
        #endregion
    }
}
