﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HL
{
    public class StrOpe
    {
        #region Shift-jisバイト数
        //Unicode→Shift-JISにしてそのバイト数
        public static int LenB(string text)
        {
            return System.Text.Encoding.GetEncoding(932).GetByteCount(text);
        }

        #endregion


        #region 文字列切断
        //'Shift-JISのバイト数で切断(頭1byteが化ける場合は開始位置を1byte戻す。ラスト1byteが化ける場合は1byte短くカット)
        public static string Substr(string text, int Start, int Length)
        {
            if (Start == 0)
            {
                return Substr(text, Length);
            }
            else
            {
                string _tmpstr = Substr(text, Start);
                if (_tmpstr == "")
                {
                    return Substr(text, Length);
                }
                else
                {
                    _tmpstr = text.Replace(_tmpstr, "");
                    return Substr(_tmpstr, Length);
                }
            }
        }
        public static string Substr(string text, int Length)
        {
            if (LenB(text) <= Length)
            {
                return text;
            }
            else
            {
                string _str = "";
                for (int I = (int)(Math.Floor((double)Length / 2.0)); I <= Length; I++)
                {
                    if (LenB(text.Substring(0, I)) > Length)
                    {
                        break;
                    }
                    _str = text.Substring(0, I);
                }
                return _str;
            }
        }

        #endregion

        #region 変換

        #region - bool
        public static bool ToBool(string Str)
        {
            try
            {
                bool B = false;
                if (bool.TryParse(Str, out B) == true)
                {
                    return B;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("文字列→整数の変換に失敗しました。\n" + Str + "\n" + ex.Message);
                return false;
            }
        }
        public static bool ToBool(int num)
        {
            try
            {
                //bool B = false;
                if (num != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("数値→整数の変換に失敗しました。\n" + num.ToString() + "\n" + ex.Message);
                return false;
            }
        }
        #endregion

        #region - int
        public static int ToInt(string Str, int tmp = 0)
        {
            try
            {
                int I = 0;
                if (int.TryParse(Str, out I) == true)
                {
                    return I;
                }
                else
                {
                    return tmp;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("文字列→整数の変換に失敗しました。\n" + Str + "\n" + tmp.ToString() + "\n" + ex.Message);
                return tmp;
            }
        }
        #endregion

        #region - DateTime
        //public static DateTime? ToDate(string Str, DateTime? tmp = null)
        //{
        //    try
        //    {
        //        DateTime? D = 0;
        //        if (DateTime.TryParse(Str, out D) == true)
        //        {
        //            return D;
        //        }
        //        else
        //        {
        //            return tmp;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("文字列→時刻の変換に失敗しました。 : \n" + Str + "\n" + tmp.ToString() + "\n" + ex.Message);
        //        return tmp;
        //    }
        //}

        /// <summary>
        /// 日次文字列を変換する
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(string dt, string format = "yyyy/MMM/dd")
        {
            DateTime rtn = DateTime.Now;

            if (DateTime.TryParseExact(dt,
                                        format,
                                        System.Globalization.DateTimeFormatInfo.InvariantInfo,
                                        System.Globalization.DateTimeStyles.None,
                                        out rtn) == false)
            {
                //VLLib.Log.Error("日時変換エラー" + dt);
            }

            return rtn;
        }

        /// <summary>
        /// 日次文字列を変換する
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static DateTime? ToDateTimeNullable(string dt, string format = "yyyy/MMM/dd")
        {
            DateTime conv;
            DateTime? rtn = DateTime.Now;

            if (dt.Length > 0)
            {
                if (DateTime.TryParseExact(dt,
                                            format,
                                            System.Globalization.DateTimeFormatInfo.InvariantInfo,
                                            System.Globalization.DateTimeStyles.None,
                                            out conv) == true)
                {
                    rtn = conv;
                }
                else
                {
                    rtn = null;
                    //VLLib.Log.Error("日時変換エラー" + dt);
                }
            }
            else
            {
                rtn = null;
            }

            return rtn;
        }

        /// <summary>
        /// 日付文字列を変換する
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static DateTime ToDate(string dt, string format = "yyyy/MMM/dd", DateTime? def = null)
        {
            DateTime rtn = DateTime.Now;

            if (dt.Length > 0)
            {
                if (DateTime.TryParseExact(dt,
                                            format,
                                            System.Globalization.DateTimeFormatInfo.InvariantInfo,
                                            System.Globalization.DateTimeStyles.None,
                                            out rtn) == false)
                {
                    //VLLib.Log.Error("日付変換エラー:" + dt);
                }
            }
            else
            {
                if (!def.HasValue)
                {
                    rtn = DateTime.MinValue;
                }
                else
                {
                    rtn = (DateTime)def;
                }
            }

            return rtn;
        }

        /// <summary>
        /// ISO8601形式の日付文字列を変換する
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static DateTime ToDateIso8601(string dt, DateTime? def = null)
        {
            DateTime rtn = DateTime.Now;
            DateTime tmp = DateTime.Now;

            if (dt.Length > 0)
            {
                if (DateTime.TryParse(dt,
                                    null,
                                    System.Globalization.DateTimeStyles.RoundtripKind,
                                    out tmp) == false)
                {
                    //VLLib.Log.Error("日付変換エラー:" + dt);
                }
            }
            else
            {
                if (!def.HasValue)
                {
                    rtn = DateTime.MinValue;
                }
                else
                {
                    rtn = (DateTime)def;
                }
            }

            // Date型への変換なので時刻は0にする
            rtn = new DateTime(tmp.Year, tmp.Month, tmp.Day);

            return rtn;
        }

        /// <summary>
        /// ISO8601形式の日付文字列を変換する
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static DateTime ToDateTimeIso8601(string dt, DateTime? def = null)
        {
            DateTime rtn = DateTime.Now;

            if (dt.Length > 0)
            {
                if (DateTime.TryParse(dt,
                                    null,
                                    System.Globalization.DateTimeStyles.RoundtripKind,
                                    out rtn) == false)
                {
                    //VLLib.Log.Error("日付変換エラー:" + dt);
                }
            }
            else
            {
                if (!def.HasValue)
                {
                    rtn = DateTime.MinValue;
                }
                else
                {
                    rtn = (DateTime)def;
                }
            }

            return rtn;
        }

        /// <summary>
        /// 日付文字列を変換する
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static DateTime? ToDateNullable(string dt, string format = "yyyy/MMM/dd", DateTime? def = null)
        {
            DateTime conv;
            DateTime? rtn = DateTime.Now;

            if (dt != null && dt.Length > 0)
            {
                if (DateTime.TryParseExact(dt,
                                            format,
                                            System.Globalization.DateTimeFormatInfo.InvariantInfo,
                                            System.Globalization.DateTimeStyles.None,
                                            out conv) == true)
                {
                    rtn = conv;
                }
                else
                {
                    rtn = null;
                    //VLLib.Log.Error("日付変換エラー:" + dt);
                }
            }
            else
            {
                rtn = (DateTime)def;
            }

            return rtn;
        }

        /// <summary>
        /// 曜日文字列取得
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToWeekDay(DateTime dt)
        {
            string weekDay = "";
            DayOfWeek dow = dt.DayOfWeek;

            // 曜日を取得
            weekDay = ToWeekDay(dow);

            return weekDay;
        }

        /// <summary>
        /// 曜日文字列取得
        /// </summary>
        /// <param name="dow"></param>
        /// <returns></returns>
        public static string ToWeekDay(DayOfWeek dow)
        {
            string weekDay = "";

            switch (dow)
            {
                case DayOfWeek.Sunday:
                    weekDay = "日";
                    break;
                case DayOfWeek.Monday:
                    weekDay = "月";
                    break;
                case DayOfWeek.Tuesday:
                    weekDay = "火";
                    break;
                case DayOfWeek.Wednesday:
                    weekDay = "水";
                    break;
                case DayOfWeek.Thursday:
                    weekDay = "木";
                    break;
                case DayOfWeek.Friday:
                    weekDay = "金";
                    break;
                case DayOfWeek.Saturday:
                    weekDay = "土";
                    break;
            }

            return weekDay;
        }
        #endregion

        #region - decimal
        public static decimal ToDec(string Str, decimal tmp = 0)
        {
            try
            {
                decimal D = 0;
                if (decimal.TryParse(Str, out D) == true)
                {
                    return D;
                }
                else
                {
                    return tmp;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("文字列→実数の変換に失敗しました。 : \n" + Str + "\n" + tmp.ToString() + "\n" + ex.Message);
                return tmp;
            }
        }
        #endregion

        #region - double
        public static double ToDbl(string Str, double tmp = 0)
        {
            try
            {
                double D = 0;
                if (double.TryParse(Str, out D) == true)
                {
                    return D;
                }
                else
                {
                    return tmp;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("文字列→実数の変換に失敗しました。 : \n" + Str + "\n" + tmp.ToString() + "\n" + ex.Message);
                return tmp;
            }
        }
        #endregion

        #region - hex
        public static string ToHexstr(int val, int keta = 0)
        {
            try
            {
                //string fom = "x";
                switch (keta)
                {
                    case 0:
                        return val.ToString("x");
                    case 2:
                        return val.ToString("x2");
                    default:
                        return val.ToString("x");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("整数→16進文字列の変換に失敗しました。 : \n" + val.ToString() + "\n" + ex.Message);
                return "";
            }
        }

        public static int HexstrToInt(string Str, int tmp = 0)
        {
            try
            {
                int I = 0;
                if (int.TryParse(Str, System.Globalization.NumberStyles.HexNumber, System.Globalization.CultureInfo.InvariantCulture, out I) == true)
                {
                    return I;
                }
                else
                {
                    return tmp;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("16進数文字列→整数の変換に失敗しました。\n" + Str + "\n" + tmp.ToString() + "\n" + ex.Message);
                return tmp;
            }
        }
        #endregion

        #endregion

        #region 文字コード判定
        /// <summary>
        /// ファイルを開いて文字コードを判定する (不明な場合はBOMなしUTF-8 (UTF-8N))
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static System.Text.Encoding DetectEncodingFromBOM(string filePath)
        {
            byte[] bs = System.IO.File.ReadAllBytes(filePath);
            System.Text.Encoding enc = DetectEncodingFromBOM(bs);
            if (enc == null)
            {
                enc = new System.Text.UTF8Encoding(false); // BOMなし
            }
            return enc;
        }

        /// <summary>
        /// BOMを調べて、文字コードを判別する。
        /// </summary>
        /// <param name="bytes">文字コードを調べるデータ。</param>
        /// <returns>BOMが見つかった時は、対応するEncodingオブジェクト。
        /// 見つからなかった時は、null。</returns>
        public static System.Text.Encoding DetectEncodingFromBOM(byte[] bytes)
        {
            if (bytes.Length < 2)
            {
                return null;
            }
            if ((bytes[0] == 0xfe) && (bytes[1] == 0xff))
            {
                //UTF-16 BE
                return new System.Text.UnicodeEncoding(true, true);
            }
            if ((bytes[0] == 0xff) && (bytes[1] == 0xfe))
            {
                if ((4 <= bytes.Length) &&
                    (bytes[2] == 0x00) && (bytes[3] == 0x00))
                {
                    //UTF-32 LE
                    return new System.Text.UTF32Encoding(false, true);
                }
                //UTF-16 LE
                return new System.Text.UnicodeEncoding(false, true);
            }
            if (bytes.Length < 3)
            {
                return null;
            }
            if ((bytes[0] == 0xef) && (bytes[1] == 0xbb) && (bytes[2] == 0xbf))
            {
                //UTF-8
                return new System.Text.UTF8Encoding(true, true);
            }
            if (bytes.Length < 4)
            {
                return null;
            }
            if ((bytes[0] == 0x00) && (bytes[1] == 0x00) &&
                (bytes[2] == 0xfe) && (bytes[3] == 0xff))
            {
                //UTF-32 BE
                return new System.Text.UTF32Encoding(true, true);
            }

            return null;
        }
        #endregion

    }
}
