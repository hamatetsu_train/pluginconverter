﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace AtsPlugin
{
    public class MainBase : EngineBase
    {
        private Process TargetProcess;
        //private AppDomain TargetAppDomain;
        //private Assembly TargetAssembly;
        private IntPtr TargetFormHandle;
        private Form TargetFormSource;
        //private MainForm TargetForm;


        private void TargetBveFinder()
        {
            TargetProcess = Process.GetCurrentProcess();
            //TargetAppDomain = AppDomain.CurrentDomain;
            //TargetAssembly = Assembly.GetEntryAssembly();
            TargetFormHandle = TargetProcess.MainWindowHandle;
            TargetFormSource = (Form)Control.FromHandle(TargetFormHandle);

            //Log.Write(TargetFormSource.Text);
        }

    }
}
