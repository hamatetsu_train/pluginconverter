﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace AtsPlugin
{
    public class AtsEngine : MainBase
    {
        #region DLL使用宣言
        // キーボード状態の取得
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern short GetKeyState(int nVirtKey);

        [DllImport("kernel32", CharSet = CharSet.Unicode, SetLastError = true)]
        internal static extern IntPtr LoadLibrary(string lpFileName);

        [DllImport("kernel32", SetLastError = true)]
        internal static extern bool FreeLibrary(IntPtr hModule);

        [DllImport("kernel32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = false)]
        internal static extern IntPtr GetProcAddress(IntPtr hModule, string lpProcName);
        #endregion

        public static AtsEngine ATS = new AtsEngine();

        #region 定義

        #region - 変数定義
        /// <summary>
        /// 列車状態
        /// </summary>
        private MyVehicleState pMyVeshicleState;

        /// <summary>
        /// ハンドル値
        /// </summary>
        AtsDefine.AtsHandles OutHandles;

        /// <summary>
        /// 設定
        /// </summary>
        private Config CFG;

        private List<DllStruct> Dlls;

        /// <summary>
        /// キーボード状態
        /// </summary>

        private short[] KeyStates;

        private bool IsDisposing;

        /// <summary>
        /// 計算用変数と代入値
        /// </summary>
        private Dictionary<string, string> Variables;

        /// <summary>
        /// 距離程と地上子
        /// </summary>
        private List<LocationBeaconDt> lLocBcn;
        #endregion

        #region - 関数デリゲート
        private delegate void LoadDelegate();
        private delegate void DisposeDelegate();
        private delegate int GetPluginVersionDelegate();
        private delegate void SetVehicleSpecDelegate(AtsDefine.AtsVehicleSpec vehicleSpec);
        private delegate void InitializeDelegate(int initialHandlePosition);
        private delegate AtsDefine.AtsHandles ElapseDelegate(AtsDefine.AtsVehicleState vehicleState, IntPtr panel, IntPtr sound);
        private delegate void SetPowerDelegate(int handlePosition);
        private delegate void SetBrakeDelegate(int handlePosition);
        private delegate void SetReverserDelegate(int handlePosition);
        private delegate void KeyDownDelegate(int keyIndex);
        private delegate void KeyUpDelegate(int keyIndex);
        private delegate void HornBlowDelegate(int hornIndex);
        private delegate void DoorOpenDelegate();
        private delegate void DoorCloseDelegate();
        private delegate void SetSignalDelegate(int signalIndex);
        private delegate void SetBeaconDataDelegate(AtsDefine.AtsBeaconData beaconData);
        private delegate void SetHandleDelegate(int PowerHandle, int BrakeHandle, int ReverserHandle, int CscValue);
        private delegate AtsDefine.AtsVehicleState GetVehicleStateDelegate();
        #endregion

        #region - クラス

        #region -- ハンドル位置保存クラス
        private class MyAtsHandles
        {
            #region 変数・プロパティ定義
            public int Power;
            public int Brake;
            public int Reverser;
            public int ConstantSpeed;

            private int pEmgBrake;

            public bool IsEmgBrake
            {
                get { if (this.Brake == this.pEmgBrake) return true; else return false; }
            }
            #endregion

            public AtsDefine.AtsHandles Handles
            {
                get { return new AtsDefine.AtsHandles() { Power = this.Power, Brake = this.Brake, Reverser = this.Reverser, ConstantSpeed = this.ConstantSpeed }; }
                set { this.Power = value.Power; this.Brake = value.Brake; this.Reverser = value.Reverser; this.ConstantSpeed = value.ConstantSpeed; }
            }

            #region コンストラクタ
            /// <summary>
            /// コンストラクタ
            /// </summary>
            public MyAtsHandles()
            {
                this.Power = 0;
                this.Brake = 0;
                this.Reverser = 0;
                this.ConstantSpeed = 0;

            }
            public MyAtsHandles(int _EmgBrake, int _InitialHandle)
            {
                this.Power = 0;
                if (_InitialHandle == 0)
                {
                    // SVC
                    this.Brake = 0;
                }
                else
                {
                    this.Brake = _EmgBrake;
                }
                this.Reverser = 0;
                this.ConstantSpeed = 0;
                this.pEmgBrake = _EmgBrake;
            }
            #endregion

        }
        #endregion

        #region -- 車両状態保存クラス
        public class MyVehicleState
        {
            #region 変数定義
            private bool pIsFirst = true;

            private AtsDefine.AtsVehicleState pNewState;
            public AtsDefine.AtsVehicleState NewState
            {
                get { return this.pNewState; }
            }

            private AtsDefine.AtsVehicleState pOldState;

            private bool pPilotLamp;
            public bool PilotLamp
            {
                get { return this.pPilotLamp; }
            }

            private double pLocation;
            public double Location
            {
                get { return Math.Abs(this.pLocation); }
            }

            /// <summary>
            /// 加速度[km/h/s]
            /// </summary>
            private float pAccelaration;
            /// <summary>
            /// 加速度[km/h/s]
            /// </summary>
            public float Accelaration
            {
                get { return this.pAccelaration; }
            }

            private float pOldAccelaration;

            #endregion

            #region コンストラクタ
            public MyVehicleState()
            {
                this.pIsFirst = true;
                this.pNewState = new AtsDefine.AtsVehicleState();
                this.pOldState = new AtsDefine.AtsVehicleState();
                this.pPilotLamp = true;
                this.pAccelaration = 0;
                this.pOldAccelaration = 0;
            }
            #endregion

            #region セット
            public void SetState(AtsDefine.AtsVehicleState vehicleState)
            {
                if (this.pIsFirst == true)
                {
                    this.pOldState = vehicleState;
                    this.pIsFirst = false;
                }
                else
                {
                    this.pOldState = this.pNewState;
                    this.pOldAccelaration = this.pAccelaration;
                    this.pAccelaration = this.pNewState.Speed - this.pOldState.Speed;
                }

                this.pNewState = vehicleState;
                this.pLocation += this.GetDeltaLocation();
            }

            /// <summary>
            /// BVE側の戸閉状態を更新
            /// </summary>
            /// <param name="_IsClose"></param>
            public void SetDoorClose(bool _IsClose)
            {
                this.pPilotLamp = _IsClose;
            }

            public void SetLocation(double _Location)
            {
                this.pLocation = _Location;
            }

            #endregion

            #region Get
            /// <summary>
            /// 前フレームとの時間差[ms]
            /// </summary>
            /// <returns></returns>
            public int GetDeltaTime()
            {
                if (this.pIsFirst == true)
                {
                    return 0;
                }
                else
                {
                    return (this.pNewState.Time - this.pOldState.Time + 60 * 60 * 24 * 1000) % (60 * 60 * 24 * 1000);
                }
            }

            /// <summary>
            /// 前フレームとの距離差[m]
            /// </summary>
            /// <returns></returns>
            public float GetDeltaLocation()
            {
                if (this.pIsFirst == true)
                {
                    return 0;
                }
                else
                {
                    return (float)(this.pNewState.Location - this.pOldState.Location);
                }
            }

            /// <summary>
            /// 前フレームとの加速度変化量[km/h/s2]
            /// </summary>
            /// <returns></returns>
            public float GetDeltaAccelaration()
            {
                if (this.pIsFirst == true)
                {
                    return 0;
                }
                else
                {
                    return (float)(this.pAccelaration - this.pOldAccelaration);
                }
            }

            /// <summary>
            /// 前回と同じ時間帯か
            /// </summary>
            /// <param name="_Interval">時間帯間隔[ms]</param>
            /// <returns></returns>
            public bool GetAnotherMoment(int _Interval)
            {
                if (_Interval > 0)
                {
                    if ((int)(this.pNewState.Time / _Interval) == (int)(this.pOldState.Time / _Interval))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            /// <summary>
            /// 指定した間隔の前半か後半か (前半ならtrue)
            /// </summary>
            /// <param name="_Interval">時間帯間隔[ms]</param>
            /// <returns></returns>
            public bool GetOddInterval(int _Interval)
            {
                if (_Interval > 1)
                {
                    if ((this.pNewState.Time % _Interval) < (int)(_Interval / 2))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }

            /// <summary>
            /// 時を取得
            /// </summary>
            /// <returns></returns>
            public int GetHour()
            {
                return (int)(this.pNewState.Time / 3600000);
            }

            /// <summary>
            /// 分を取得
            /// </summary>
            /// <returns></returns>
            public int GetMinute()
            {
                return (int)((int)(this.pNewState.Time / 1000) / 60) % 60;
            }

            /// <summary>
            /// 秒を取得
            /// </summary>
            /// <returns></returns>
            public int GetSecond()
            {
                return (int)(this.pNewState.Time / 1000) % 60;
            }

            public int GetBc()
            {
                return (int)(this.pNewState.BcPressure);
            }

            public int GetMr()
            {
                return (int)(this.pNewState.MrPressure);
            }

            #endregion
        }
        #endregion

        #region -- DLL管理クラス
        private class DllStruct
        {
            #region 定義
            public Config.CFG_DLL CFG;
            public IntPtr handle;
            public bool NFB;

            #region - 関数ポインタ
            private IntPtr loadAddress;
            private IntPtr disposeAddress;
            private IntPtr getPluginVersionAddress;
            private IntPtr setVehicleSpecAddress;
            private IntPtr initializeAddress;
            private IntPtr elapseAddress;
            private IntPtr setPowerAddress;
            private IntPtr setBrakeAddress;
            private IntPtr setReverserAddress;
            private IntPtr keyDownAddress;
            private IntPtr keyUpAddress;
            private IntPtr hornBlowAddress;
            private IntPtr doorOpenAddress;
            private IntPtr doorCloseAddress;
            private IntPtr setSignalAddress;
            private IntPtr setBeaconDataAddress;
            private IntPtr setHandleAddress;
            private IntPtr getVehicleStateAddress;
            #endregion

            #region - 関数デリゲートインスタンス
            public LoadDelegate loadDelegate = null;
            public DisposeDelegate disposeDelegate = null;
            public GetPluginVersionDelegate getPluginVersionDelegate = null;
            public SetVehicleSpecDelegate setVehicleSpecDelegate = null;
            public InitializeDelegate initializeDelegate = null;
            public ElapseDelegate elapseDelegate = null;
            public SetPowerDelegate setPowerDelegate = null;
            public SetBrakeDelegate setBrakeDelegate = null;
            public SetReverserDelegate setReverserDelegate = null;
            public KeyDownDelegate keyDownDelegate = null;
            public KeyUpDelegate keyUpDelegate = null;
            public HornBlowDelegate hornBlowDelegate = null;
            public DoorOpenDelegate doorOpenDelegate = null;
            public DoorCloseDelegate doorCloseDelegate = null;
            public SetSignalDelegate setSignalDelegate = null;
            public SetBeaconDataDelegate setBeaconDataDelegate = null;
            public SetHandleDelegate setHandleDelegate = null;
            public GetVehicleStateDelegate getVehicleStateDelegate = null;
            #endregion

            #endregion

            #region コンストラクタ
            public DllStruct(Config.CFG_DLL pCFG)
            {
                try
                {
                    if (pCFG != null)
                    {
                        this.CFG = pCFG;
                    }

                    if (this.CFG != null && System.IO.File.Exists(this.CFG.DllPath) == true)
                    {
                        this.handle = LoadLibrary(this.CFG.DllPath);

                        if (this.handle != null && this.handle != IntPtr.Zero)
                        {
                            Log.Error("[DllLoad] : 読込成功 : " + pCFG.DllPath);

                            this.NFB = this.CFG.InitializeNfbOn;

                            this.loadAddress = GetProcAddress(handle, "Load");
                            if (this.loadAddress != null && this.loadAddress != IntPtr.Zero)
                            {
                                this.loadDelegate = (LoadDelegate)Marshal.GetDelegateForFunctionPointer(this.loadAddress, typeof(LoadDelegate));
#if DEBUG
                                Log.Debug("[DllLoad] : Load");
#endif
                            }
                            else
                            {
                                Log.Error("[DllLoad] : -> 不明 : Load");
                            }

                            this.disposeAddress = GetProcAddress(handle, "Dispose");
                            if (this.disposeAddress != null && this.disposeAddress != IntPtr.Zero)
                            {
                                this.disposeDelegate = (DisposeDelegate)Marshal.GetDelegateForFunctionPointer(this.disposeAddress, typeof(DisposeDelegate));
#if DEBUG
                                Log.Debug("[DllLoad] : Dispose");
#endif
                            }
                            else
                            {
                                Log.Error("[DllLoad] : -> 不明 : Dispose");
                            }

                            this.getPluginVersionAddress = GetProcAddress(handle, "GetPluginVersion");
                            if (this.getPluginVersionAddress != null && this.getPluginVersionAddress != IntPtr.Zero)
                            {
                                this.getPluginVersionDelegate = (GetPluginVersionDelegate)Marshal.GetDelegateForFunctionPointer(this.getPluginVersionAddress, typeof(GetPluginVersionDelegate));
#if DEBUG
                                Log.Debug("[DllLoad] : GetPluginVersion");
#endif
                            }
                            else
                            {
                                Log.Error("[DllLoad] : -> 不明 : GetPluginVersion");
                            }

                            this.setVehicleSpecAddress = GetProcAddress(handle, "SetVehicleSpec");
                            if (this.setVehicleSpecAddress != null && this.setVehicleSpecAddress != IntPtr.Zero)
                            {
                                this.setVehicleSpecDelegate = (SetVehicleSpecDelegate)Marshal.GetDelegateForFunctionPointer(this.setVehicleSpecAddress, typeof(SetVehicleSpecDelegate));
#if DEBUG
                                Log.Debug("[DllLoad] : SetVehicleSpec");
#endif
                            }
                            else
                            {
                                Log.Error("[DllLoad] : -> 不明 : SetVehicleSpec");
                            }

                            this.initializeAddress = GetProcAddress(handle, "Initialize");
                            if (this.initializeAddress != null && this.initializeAddress != IntPtr.Zero)
                            {
                                this.initializeDelegate = (InitializeDelegate)Marshal.GetDelegateForFunctionPointer(this.initializeAddress, typeof(InitializeDelegate));
#if DEBUG
                                Log.Debug("[DllLoad] : Initialize");
#endif
                            }
                            else
                            {
                                Log.Error("[DllLoad] : -> 不明 : Initialize");
                            }

                            this.elapseAddress = GetProcAddress(handle, "Elapse");
                            if (this.elapseAddress != null && this.elapseAddress != IntPtr.Zero)
                            {
                                this.elapseDelegate = (ElapseDelegate)Marshal.GetDelegateForFunctionPointer(this.elapseAddress, typeof(ElapseDelegate));
#if DEBUG
                                Log.Debug("[DllLoad] : Elapse");
#endif
                            }
                            else
                            {
                                Log.Error("[DllLoad] : -> 不明 : Elapse");
                            }

                            this.setPowerAddress = GetProcAddress(handle, "SetPower");
                            if (this.setPowerAddress != null && this.setPowerAddress != IntPtr.Zero)
                            {
                                this.setPowerDelegate = (SetPowerDelegate)Marshal.GetDelegateForFunctionPointer(this.setPowerAddress, typeof(SetPowerDelegate));
#if DEBUG
                                Log.Debug("[DllLoad] : SetPower");
#endif
                            }
                            else
                            {
                                Log.Error("[DllLoad] : -> 不明 : SetPower");
                            }

                            this.setBrakeAddress = GetProcAddress(handle, "SetBrake");
                            if (this.setBrakeAddress != null && this.setBrakeAddress != IntPtr.Zero)
                            {
                                this.setBrakeDelegate = (SetBrakeDelegate)Marshal.GetDelegateForFunctionPointer(this.setBrakeAddress, typeof(SetBrakeDelegate));
#if DEBUG
                                Log.Debug("[DllLoad] : SetBrake");
#endif
                            }
                            else
                            {
                                Log.Error("[DllLoad] : -> 不明 : SetBrake");
                            }

                            this.setReverserAddress = GetProcAddress(handle, "SetReverser");
                            if (this.setReverserAddress != null && this.setReverserAddress != IntPtr.Zero)
                            {
                                this.setReverserDelegate = (SetReverserDelegate)Marshal.GetDelegateForFunctionPointer(this.setReverserAddress, typeof(SetReverserDelegate));
#if DEBUG
                                Log.Debug("[DllLoad] : SetReverser");
#endif
                            }
                            else
                            {
                                Log.Error("[DllLoad] : -> 不明 : SetReverser");
                            }

                            this.keyDownAddress = GetProcAddress(handle, "KeyDown");
                            if (this.keyDownAddress != null && this.keyDownAddress != IntPtr.Zero)
                            {
                                this.keyDownDelegate = (KeyDownDelegate)Marshal.GetDelegateForFunctionPointer(this.keyDownAddress, typeof(KeyDownDelegate));
#if DEBUG
                                Log.Debug("[DllLoad] : KeyDown");
#endif
                            }
                            else
                            {
                                Log.Error("[DllLoad] : -> 不明 : KeyDown");
                            }

                            this.keyUpAddress = GetProcAddress(handle, "KeyUp");
                            if (this.keyUpAddress != null && this.keyUpAddress != IntPtr.Zero)
                            {
                                this.keyUpDelegate = (KeyUpDelegate)Marshal.GetDelegateForFunctionPointer(this.keyUpAddress, typeof(KeyUpDelegate));
#if DEBUG
                                Log.Debug("[DllLoad] : KeyUp");
#endif
                            }
                            else
                            {
                                Log.Error("[DllLoad] : -> 不明 : KeyUp");
                            }

                            this.hornBlowAddress = GetProcAddress(handle, "HornBlow");
                            if (this.hornBlowAddress != null && this.hornBlowAddress != IntPtr.Zero)
                            {
                                this.hornBlowDelegate = (HornBlowDelegate)Marshal.GetDelegateForFunctionPointer(this.hornBlowAddress, typeof(HornBlowDelegate));
#if DEBUG
                                Log.Debug("[DllLoad] : HornBlow");
#endif
                            }
                            else
                            {
                                Log.Error("[DllLoad] : -> 不明 : HornBlow");
                            }

                            this.doorOpenAddress = GetProcAddress(handle, "DoorOpen");
                            if (this.doorOpenAddress != null && this.doorOpenAddress != IntPtr.Zero)
                            {
                                this.doorOpenDelegate = (DoorOpenDelegate)Marshal.GetDelegateForFunctionPointer(this.doorOpenAddress, typeof(DoorOpenDelegate));
#if DEBUG
                                Log.Debug("[DllLoad] : DoorOpen");
#endif
                            }
                            else
                            {
                                Log.Error("[DllLoad] : -> 不明 : DoorOpen");
                            }

                            this.doorCloseAddress = GetProcAddress(handle, "DoorClose");
                            if (this.doorCloseAddress != null && this.doorCloseAddress != IntPtr.Zero)
                            {
                                this.doorCloseDelegate = (DoorCloseDelegate)Marshal.GetDelegateForFunctionPointer(this.doorCloseAddress, typeof(DoorCloseDelegate));
#if DEBUG
                                Log.Debug("[DllLoad] : DoorClose");
#endif
                            }
                            else
                            {
                                Log.Error("[DllLoad] : -> 不明 : DoorClose");
                            }

                            this.setSignalAddress = GetProcAddress(handle, "SetSignal");
                            if (this.setSignalAddress != null && this.setSignalAddress != IntPtr.Zero)
                            {
                                this.setSignalDelegate = (SetSignalDelegate)Marshal.GetDelegateForFunctionPointer(this.setSignalAddress, typeof(SetSignalDelegate));
#if DEBUG
                                Log.Debug("[DllLoad] : SetSignal");
#endif
                            }
                            else
                            {
                                Log.Error("[DllLoad] : -> 不明 : SetSignal");
                            }

                            this.setBeaconDataAddress = GetProcAddress(handle, "SetBeaconData");
                            if (this.setBeaconDataAddress != null && this.setBeaconDataAddress != IntPtr.Zero)
                            {
                                this.setBeaconDataDelegate = (SetBeaconDataDelegate)Marshal.GetDelegateForFunctionPointer(this.setBeaconDataAddress, typeof(SetBeaconDataDelegate));
#if DEBUG
                                Log.Debug("[DllLoad] : SetBeaconData");
#endif
                            }
                            else
                            {
                                Log.Error("[DllLoad] : -> 不明 : SetBeaconData");
                            }

                            this.setHandleAddress = GetProcAddress(handle, "SetHandles");
                            if (this.CFG.HasSetHandle == true)
                            {
                                if (this.setHandleAddress != null && this.setHandleAddress != IntPtr.Zero)
                                {
                                    this.setHandleDelegate = (SetHandleDelegate)Marshal.GetDelegateForFunctionPointer(this.setHandleAddress, typeof(SetHandleDelegate));
#if DEBUG
                                Log.Debug("[DllLoad] : SetHandles");
#endif
                                }
                                else
                                {
                                    Log.Error("[DllLoad] : -> 不明 : SetHandles");
                                }
                            }

                            this.getVehicleStateAddress = GetProcAddress(handle, "GetVehicleState");
                            if (this.CFG.HasGetVehicleState == true)
                            {
                                if (this.getVehicleStateAddress != null && this.getVehicleStateAddress != IntPtr.Zero)
                                {
                                    this.getVehicleStateDelegate = (GetVehicleStateDelegate)Marshal.GetDelegateForFunctionPointer(this.getVehicleStateAddress, typeof(GetVehicleStateDelegate));
#if DEBUG
                                Log.Debug("[DllLoad] : GetVehicleState");
#endif
                                }
                                else
                                {
                                    Log.Error("[DllLoad] : -> 不明 : GetVehicleState");
                                }
                            }
                            Log.Debug("    (Get) : H=" + this.CFG.GetHandle.ToString() + " P=" + this.CFG.GetPower.ToString() + " B=" + this.CFG.GetBrake.ToString() + " R=" + this.CFG.GetReverser.ToString());
                            Log.Debug("    (Set) : P=" + this.CFG.SetPower.ToString() + " B=" + this.CFG.SetBrake.ToString() + " R=" + this.CFG.SetReverser.ToString() + " C=" + this.CFG.SetCsc.ToString());
                        }
                        else
                        {
                            Init();
                            Log.Error("[DllLoad] : 読込失敗 : " + pCFG.DllPath);
                        }
                    }
                    else
                    {
                        Init();
                        Log.Error("[DllLoad] : 不明なパス : " + pCFG.DllPath);
                    }
                }
                catch (Exception ex)
                {
                    this.handle = IntPtr.Zero;
                    this.NFB = false;
                    Log.Error("[DllLoad] : " + pCFG.DllPath + " : " + ex.Message, ex);
                }
            }
#endregion

            #region デストラクタ
            public void Dispose()
            {
                try
                {
                    if (this.handle != null && this.handle != IntPtr.Zero)
                    {
                        FreeLibrary(this.handle);
                        Log.Info("[DllDispose] : 解放成功");
                    }
                }
                catch (Exception ex)
                {
                    Log.Error("[DllDispose] : " + ex.Message);
                }
            }
            #endregion

            #region 初期化
            public void Init()
            {
                this.NFB = false;
                this.handle = IntPtr.Zero;
                this.loadAddress = IntPtr.Zero;
                this.disposeAddress = IntPtr.Zero;
                this.getPluginVersionAddress = IntPtr.Zero;
                this.setVehicleSpecAddress = IntPtr.Zero;
                this.initializeAddress = IntPtr.Zero;
                this.elapseAddress = IntPtr.Zero;
                this.setPowerAddress = IntPtr.Zero;
                this.setBrakeAddress = IntPtr.Zero;
                this.setReverserAddress = IntPtr.Zero;
                this.keyDownAddress = IntPtr.Zero;
                this.keyUpAddress = IntPtr.Zero;
                this.hornBlowAddress = IntPtr.Zero;
                this.doorOpenAddress = IntPtr.Zero;
                this.doorCloseAddress = IntPtr.Zero;
                this.setSignalAddress = IntPtr.Zero;
                this.setBeaconDataAddress = IntPtr.Zero;
                this.setHandleAddress = IntPtr.Zero;
                this.getVehicleStateAddress = IntPtr.Zero;
            }
            #endregion
        }
#endregion

        #region -- 距離と地上子
        private class LocationBeaconDt
        {
            public double Location { get; set; }
            public AtsDefine.AtsBeaconData BeaconData { get; set; }
        }

        #endregion

#endregion

#endregion


        #region コンストラクタ
        public AtsEngine()
        {
            try
            {
                // ログ初期化
                Log.Init();
                Log.Write("[Construct] PluginConverter " + ModuleVersion);

                // 初期化
                Init();
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainConstruct]", true);
            }
        }
        #endregion

        #region 初期化
        public void Init()
        {
            try
            {
#if DEBUG
                Log.Debug("[Init] - Start");
#endif
                this.pMyVeshicleState = new MyVehicleState();
                this.OutHandles = new AtsDefine.AtsHandles() { Power = 0, Brake = 0, ConstantSpeed = AtsDefine.AtsCscInstruction.Continue, Reverser = 0 };

                this.CFG = new Config();

                this.Dlls = new List<DllStruct>();
                this.KeyStates = new short[256];
                this.IsDisposing = false;
                this.Variables = new Dictionary<string, string>();
                this.lLocBcn = new List<LocationBeaconDt>();
#if DEBUG
                Log.Debug("[Init] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainInit]", true);
            }
        }
        #endregion


        // _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/
        // ココカラ
        // _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/ _/

        #region Load
        public void Load()
        {
            string dllName = "null";
            try
            {
#if DEBUG
                Log.Write("[Load] - Start");
#endif
                // 設定ロード
                if (this.CFG.MainLoad() == false)
                {
#if DEBUG
                    Log.Write("[Load] - 失敗");
#endif
                }
                Log.SetLevel(this.CFG.General.LogLevel);
                this.IsDisposing = false;

                // DLL読み込み
                if (this.CFG.Dll != null && this.CFG.Dll.Count > 0)
                {
                    foreach(Config.CFG_DLL onedll in this.CFG.Dll)
                    {
                        dllName = "(1)"+System.IO.Path.GetFileName(onedll.DllPath);
                        DllStruct dllstruct = new DllStruct(onedll);
                        if (dllstruct.handle != IntPtr.Zero)
                        {
                            // 読み込み成功ならリストに追加
                            this.Dlls.Add(dllstruct);
                        }
                    }
                }

                // 各DLL処理
                if (this.Dlls != null && this.Dlls.Count > 0)
                {
                    foreach (DllStruct dllstruct in this.Dlls)
                    {
                        dllName = "(2)" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath);
                        if (dllstruct.handle != IntPtr.Zero && dllstruct.loadDelegate != null)
                        {
                            try
                            {
                                dllstruct.loadDelegate();
                            }
                            catch (Exception ex3)
                            {
                                ExceptionMessage(ex3, "[MainLoad]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + ">", true);
                                // Loadで例外が発生した時点で使用不可にする
                                dllstruct.Init();
                            }
                        }
                    }
                }

#if DEBUG
                Log.Write("[Load] - End");
#endif

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainLoad]<" + dllName + ">", true);
            }
        }
        #endregion

        #region Dispose
        public void Dispose()
        {
            try
            {
#if DEBUG
                Log.Write("[MainDispose] - Start");
#endif
                this.IsDisposing = true;

                // 各DLL処理
                if (this.Dlls != null && this.Dlls.Count > 0)
                {
                    foreach (DllStruct dllstruct in this.Dlls)
                    {
                        if (dllstruct.handle != IntPtr.Zero && dllstruct.disposeDelegate != null)
                        {
                            try
                            {
                                dllstruct.disposeDelegate();
                            }
                            catch (Exception ex2)
                            {
                                ExceptionMessage(ex2, "[MainDispose]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + ">", true);
                            }
                        }
                    }
                }

                // DLL開放
                if (this.Dlls != null && this.Dlls.Count > 0)
                {
                    foreach (DllStruct dllstruct in this.Dlls)
                    {
                        if (dllstruct.handle != IntPtr.Zero)
                        {
                            dllstruct.Dispose();
                        }
                    }
                }

                // ログ片付け
                Log.Write("[MainDispose]");
                Log.Dispose();

                // リストを空にする。そのままリロードされて再利用されることもあるから
                this.CFG.Dll = new List<Config.CFG_DLL>();
                this.Dlls = new List<DllStruct>();
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainDispose]", true);
            }
        }
        #endregion

        #region GetPluginVersion
        /// <summary>
        /// ※おおもとで代表で返しているのでここはvoidにしとく
        /// </summary>
        public void GetPluginVersion()
        {
            try
            {
#if DEBUG
                Log.Debug("[GetPluginVersion] - Start");
#endif
                // 各DLL処理
                if (this.Dlls != null && this.Dlls.Count > 0)
                {
                    foreach (DllStruct dllstruct in this.Dlls)
                    {
                        if (dllstruct.handle != IntPtr.Zero && dllstruct.getPluginVersionDelegate != null)
                        {
                            try
                            {
                                int version = dllstruct.getPluginVersionDelegate();
                            }
                            catch (Exception ex2)
                            {
                                ExceptionMessage(ex2, "[GetPluginVersion]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + ">", true);
                            }
                        }
                    }
                }

#if DEBUG
                Log.Debug("[GetPluginVersion] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainGetPluginVersion]", true);
            }
        }
        #endregion

        #region SetVehicleSpec
        public void SetVehicleSpec(AtsDefine.AtsVehicleSpec vehicleSpec)
        {
            try
            {
#if DEBUG
                Log.Debug("[SetVehicleSpec] - Start");
#endif
                // 自分用に保存
                this.CFG.VehicleSpec.SetSpec(vehicleSpec);

                // 初期化
                this.OutHandles = new AtsDefine.AtsHandles() { Power = 0, Brake = this.CFG.VehicleSpec.EmgBrakeNotch, ConstantSpeed = AtsDefine.AtsCscInstruction.Continue, Reverser = 0 };

                // 各DLL処理
                if (this.Dlls != null && this.Dlls.Count > 0)
                {
                    foreach (DllStruct dllstruct in this.Dlls)
                    {
                        if (dllstruct.handle != IntPtr.Zero && dllstruct.setVehicleSpecDelegate != null)
                        {
                            try
                            {
                                dllstruct.setVehicleSpecDelegate(vehicleSpec);
                            }
                            catch (Exception ex2)
                            {
                                ExceptionMessage(ex2, "[MainSetVehicleSpec]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + ">", true);
                            }
                        }
                    }
                }
#if DEBUG
                Log.Debug("[SetVehicleSpec] - End");
#endif

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetVehicleSpec]", true);
            }
        }
        #endregion

        #region Initialize
        public void Initialize(int initialHandlePosition)
        {
            try
            {
#if DEBUG
                Log.Debug("[Initialize] - Start : " + initialHandlePosition.ToString());
#endif
                // 各DLL処理
                if (this.Dlls != null && this.Dlls.Count > 0)
                {
                    foreach (DllStruct dllstruct in this.Dlls)
                    {
                        if (dllstruct.handle != IntPtr.Zero && dllstruct.initializeDelegate != null)
                        {
                            try
                            {
                                dllstruct.initializeDelegate(initialHandlePosition);
                            }
                            catch (Exception ex2)
                            {
                                ExceptionMessage(ex2, "[MainInitialize]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + ">", true);
                            }
                        }
                    }
                }
#if DEBUG
                Log.Debug("[Initialize] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainInitialize]", true);
            }
        }
        #endregion

        #region Elapse

        #region - Main
        public AtsDefine.AtsHandles Elapse(AtsDefine.AtsVehicleState vehicleState, IntPtr panel, IntPtr sound)
        {
            try
            {
#if DEBUG
                Log.Debug("[Elapse] - Start");
#endif
                // 列車状態
                this.pMyVeshicleState.SetState(vehicleState);

                // キーボード入力チェック
                this.CheckKeyState();

                // 各DLL処理
                //bool FirstElapse = true;
                if (this.IsDisposing == false && this.Dlls != null && this.Dlls.Count > 0)
                {
                    for (int i=0; i<this.Dlls.Count; i++)
                    {
                        DllStruct dllstruct = this.Dlls[i];
                        if (dllstruct.NFB == true && dllstruct.handle != IntPtr.Zero && dllstruct.elapseDelegate != null)
                        {
                            //if (FirstElapse == true)
                            //{
                            //    // 前フレームでの最終値を渡す
                            //    if (dllstruct.CFG.GetHandle == true && dllstruct.CFG.HasSetHandle == true && dllstruct.setHandleDelegate != null)
                            //    {
                            //        // SetHandles関数があるならそれに渡す
                            //        SetHandles(i, OutHandles.Power, OutHandles.Brake, OutHandles.Reverser, OutHandles.ConstantSpeed);
                            //    }
                            //    else
                            //    {
                            //        if (dllstruct.CFG.GetPower == true && dllstruct.setPowerDelegate != null)
                            //        {
                            //            SetPower(i, OutHandles.Power);
                            //        }
                            //        if (dllstruct.CFG.GetBrake == true && dllstruct.setBrakeDelegate != null)
                            //        {
                            //            SetBrake(i, OutHandles.Brake);
                            //        }
                            //        if (dllstruct.CFG.GetReverser == true && dllstruct.setReverserDelegate != null)
                            //        {
                            //            SetReverser(i, OutHandles.Reverser);
                            //        }
                            //    }
                            //    FirstElapse = false;
                            //}

                            // 実行
                            try
                            {
                                AtsDefine.AtsHandles rtnHandles = dllstruct.elapseDelegate(this.pMyVeshicleState.NewState, panel, sound);
                                Log.Debug("[Elapse] (" + i.ToString() + ") : P=" + rtnHandles.Power.ToString() + ", B=" + rtnHandles.Brake.ToString() + ", R=" + rtnHandles.Reverser.ToString() + ", C=" + rtnHandles.ConstantSpeed.ToString());

                                // 結果をセット
                                if (dllstruct.CFG.SetPower == true)
                                {
                                    OutHandles.Power = rtnHandles.Power;
                                }
                                if (dllstruct.CFG.SetBrake == true)
                                {
                                    OutHandles.Brake = rtnHandles.Brake;
                                }
                                if (dllstruct.CFG.SetReverser == true)
                                {
                                    OutHandles.Reverser = rtnHandles.Reverser;
                                }
                                if (dllstruct.CFG.SetCsc == true)
                                {
                                    OutHandles.ConstantSpeed = rtnHandles.ConstantSpeed;
                                }


                                // 後続のDLLにハンドル位置を代入
                                for (int j = i + 1; j < this.Dlls.Count; j++)
                                {
                                    DllStruct dllstruct2 = this.Dlls[j];
                                    if (dllstruct2.NFB == true && dllstruct2.handle != IntPtr.Zero)
                                    {
                                        if (dllstruct2.CFG.GetHandle == true && dllstruct2.CFG.HasSetHandle == true && dllstruct2.setHandleDelegate != null)
                                        {
                                            // SetHandles関数があるならそれに渡す
                                            SetHandles(j, OutHandles.Power, OutHandles.Brake, OutHandles.Reverser, OutHandles.ConstantSpeed);
                                        }
                                        else
                                        {
                                            if (dllstruct2.CFG.GetPower == true && dllstruct2.setPowerDelegate != null)
                                            {
                                                SetPower(j, OutHandles.Power);
                                            }
                                            if (dllstruct2.CFG.GetBrake == true && dllstruct2.setBrakeDelegate != null)
                                            {
                                                SetBrake(j, OutHandles.Brake);
                                            }
                                            if (dllstruct2.CFG.GetReverser == true && dllstruct2.setReverserDelegate != null)
                                            {
                                                SetReverser(j, OutHandles.Reverser);
                                            }
                                        }
                                    }

                                }
                            }
                            catch (Exception ex2)
                            {
                                ExceptionMessageOnce(ex2, "[MainElapse]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + ">", true);
                            }
                        }
                        else
                        {
                            Log.Debug("[Elapse] (" + i.ToString() + ") : NFB OFF");
                        }

                        if (dllstruct.CFG.HasGetVehicleState == true && dllstruct.getVehicleStateDelegate != null)
                        {
                            this.pMyVeshicleState.SetState(dllstruct.getVehicleStateDelegate());
                        }
                    }

                    // 後回しにしていた地上子を発火
                    for(int j = 0; j < this.lLocBcn.Count; j++)
                    {
                        if (this.lLocBcn[j].Location < vehicleState.Location)
                        {
                            for (int i = 0; i < this.Dlls.Count; i++)
                            {
                                DllStruct dllstruct = this.Dlls[i];
                                if (dllstruct.NFB == true && dllstruct.handle != IntPtr.Zero && dllstruct.setBeaconDataDelegate != null)
                                {
                                    // 実行
                                    try
                                    {
                                        dllstruct.setBeaconDataDelegate(this.lLocBcn[j].BeaconData);
                                        Log.Debug("[Elapse] - Beacon : Loc=" + vehicleState.Location.ToString("0.000") + ", Type=" + this.lLocBcn[j].BeaconData.Type.ToString() + ", Len=" + this.lLocBcn[j].BeaconData.Distance.ToString() + ", Sig=" + this.lLocBcn[j].BeaconData.Signal.ToString() + ", Data=" + this.lLocBcn[j].BeaconData.Optional.ToString());
                                    }
                                    catch (Exception ex3)
                                    {
                                        ExceptionMessageOnce(ex3, "[MainElapse]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + " - Beacon>", true);
                                    }
#if DEBUG
                                    //Log.Write("[Elapse] (" + i.ToString() + ") : P=" + rtnHandles.Power.ToString() + ", B=" + rtnHandles.Brake.ToString() + ", R=" + rtnHandles.Reverser.ToString() + ", C=" + rtnHandles.ConstantSpeed.ToString());
#endif
                                }
                            }
                        }
                    }
                    // 実行したものを削除
                    this.lLocBcn.RemoveAll(p => p.Location < vehicleState.Location);

                }
                else
                {
                    Log.Debug("[Elapse] (IsDisposing or Dll.Count=0)");
                }
                //                if (FirstElapse == true)
                //                {
                //                    // falseではない→１つも動作してない→禁則事項です
                //                    this.OutHandles = new AtsMain.AtsHandles() { Power = 0, Brake = this.CFG.VehicleSpec.EmgBrakeNotch, ConstantSpeed = AtsMain.AtsCscInstruction.Continue, Reverser = 0 };
#if DEBUG
                //                    Log.Write("[Elapse] - 動作DLLなし");
#endif
                //                }
                Log.Debug("[Elapse] - End : P=" + this.OutHandles.Power.ToString() + ", B=" + this.OutHandles.Brake.ToString() + ", R=" + this.OutHandles.Reverser.ToString() + ", C=" + this.OutHandles.ConstantSpeed.ToString());
            }
            catch (Exception ex)
            {
                ExceptionMessageOnce(ex, "[MainElapse]", true);
            }

            return this.OutHandles;
        }
        #endregion

        #endregion

        #region SetPower
        public void SetPower(int handlePosition)
        {
            try
            {
#if DEBUG
                Log.Debug("[SetPower] - Start : " + handlePosition.ToString());
#endif
                // 各DLL処理
                if (this.IsDisposing == false && this.Dlls != null && this.Dlls.Count > 0)
                {
                    for (int i=0; i<this.Dlls.Count; i++)
                    {
                        SetPower(i, handlePosition);
                    }
                }
#if DEBUG
                Log.Debug("[SetPower] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetPower]", true);
            }
        }
        public void SetPower(int dllIndex, int handlePosition)
        {
            try
            {
#if DEBUG
                Log.Debug("[SetPower] - Start : " + dllIndex.ToString() + ", " + handlePosition.ToString());
#endif
                // 各DLL処理
                if (this.Dlls != null && this.Dlls.Count > dllIndex)
                {
                    DllStruct dllstruct = this.Dlls[dllIndex];
                    if (dllstruct.handle != IntPtr.Zero && dllstruct.setPowerDelegate != null)
                    {
                        try
                        {
                            dllstruct.setPowerDelegate(handlePosition);
                        }
                        catch (Exception ex2)
                        {
                            ExceptionMessage(ex2, "[MainSetPower]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + ">", true);
                        }
                    }
                }
#if DEBUG
                Log.Debug("[SetPower] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetPower]", true);
            }
        }
        #endregion

        #region SetBrake
        public void SetBrake(int handlePosition)
        {
            try
            {
#if DEBUG
                Log.Debug("[SetBrake] - Start : " + handlePosition.ToString());
#endif
                // 各DLL処理
                if (this.IsDisposing == false && this.Dlls != null && this.Dlls.Count > 0)
                {
                    for (int i = 0; i < this.Dlls.Count; i++)
                    {
                        SetBrake(i, handlePosition);
                    }
                }
#if DEBUG
                Log.Debug("[SetBrake] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetBrake]", true);
            }
        }
        public void SetBrake(int dllIndex, int handlePosition)
        {
            try
            {
#if DEBUG
                Log.Debug("[SetBrake] - Start : " + dllIndex.ToString() + ", " + handlePosition.ToString());
#endif
                // 各DLL処理
                if (this.Dlls != null && this.Dlls.Count > dllIndex)
                {
                    DllStruct dllstruct = this.Dlls[dllIndex];
                    if (dllstruct.handle != IntPtr.Zero && dllstruct.setBrakeDelegate != null)
                    {
                        try
                        {
                            dllstruct.setBrakeDelegate(handlePosition);
                        }
                        catch (Exception ex2)
                        {
                            ExceptionMessage(ex2, "[MainSetBrake]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + ">", true);
                        }
                    }
                }
#if DEBUG
                Log.Debug("[SetBrake] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetBrake]", true);
            }
        }
        #endregion

        #region SetReverser
        public void SetReverser(int handlePosition)
        {
            try
            {
#if DEBUG
                Log.Debug("[SetReverser] - Start : " + handlePosition.ToString());
#endif
                // 各DLL処理
                if (this.IsDisposing == false && this.Dlls != null && this.Dlls.Count > 0)
                {
                    for (int i = 0; i < this.Dlls.Count; i++)
                    {
                        SetReverser(i, handlePosition);
                    }
                }
#if DEBUG
                Log.Debug("[SetReverser] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetReverser]", true);
            }
        }
        public void SetReverser(int dllIndex, int handlePosition)
        {
            try
            {
#if DEBUG
                Log.Debug("[SetReverser] - Start : " + dllIndex.ToString() + ", " + handlePosition.ToString());
#endif
                // 各DLL処理
                if (this.Dlls != null && this.Dlls.Count > dllIndex)
                {
                    DllStruct dllstruct = this.Dlls[dllIndex];
                    if (dllstruct.handle != IntPtr.Zero && dllstruct.setReverserDelegate != null)
                    {
                        try
                        {
                            dllstruct.setReverserDelegate(handlePosition);
                        }
                        catch (Exception ex2)
                        {
                            ExceptionMessage(ex2, "[MainSetReverser]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + ">", true);
                        }
                    }
                }
#if DEBUG
                Log.Debug("[SetReverser] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetReverser]", true);
            }
        }
        #endregion

        #region キーイベント

        #region KeyDown
        public void KeyDown(int keyIndex)
        {
            try
            {
#if DEBUG
                Log.Debug("[KeyDown] - Start : " + keyIndex.ToString());
#endif
                if (this.IsDisposing == false)
                {
                    // NFBチェック
                    CheckKeyPush(Config.KeyInput_ENUM.AtsKey, keyIndex);

                    // 各DLL処理
                    if (this.Dlls != null && this.Dlls.Count > 0)
                    {
                        foreach (DllStruct dllstruct in this.Dlls)
                        {
                            if (dllstruct.handle != IntPtr.Zero && dllstruct.keyDownDelegate != null)
                            {
                                try
                                {
                                    dllstruct.keyDownDelegate(keyIndex);
                                }
                                catch (Exception ex2)
                                {
                                    ExceptionMessage(ex2, "[MainKeyDown]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + ">", true);
                                }
                            }
                        }
                    }
                }
#if DEBUG
                Log.Debug("[KeyDown] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainKeyDown]", true);
            }
        }
        #endregion

        #region KeyUp
        public void KeyUp(int keyIndex)
        {
            try
            {
#if DEBUG
                Log.Debug("[KeyUp] - Start : " + keyIndex.ToString());
#endif
                if (this.IsDisposing == false)
                {
                    // キーチェック
                    CheckKeyRelease(Config.KeyInput_ENUM.AtsKey, keyIndex);

                    // 各DLL処理
                    if (this.Dlls != null && this.Dlls.Count > 0)
                    {
                        foreach (DllStruct dllstruct in this.Dlls)
                        {
                            if (dllstruct.handle != IntPtr.Zero && dllstruct.keyUpDelegate != null)
                            {
                                try
                                {
                                    dllstruct.keyUpDelegate(keyIndex);
                                }
                                catch (Exception ex2)
                                {
                                    ExceptionMessage(ex2, "[MainKeyUp]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + ">", true);
                                }
                            }
                        }
                    }
                }
#if DEBUG
                Log.Debug("[KeyUp] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainKeyUp]", true);
            }
        }
        #endregion

        #region - キーボード
        private void CheckKeyState()
        {
            try
            {
                foreach (int keyCode in this.CFG.KeyCodes)
                {
                    // キーが押されていたら (最上位ビットが1になる->signed shortなので負の数になる)
                    short KS = GetKeyState(keyCode);
                    if (this.KeyStates[keyCode] >= 0 && KS < 0)
                    {
                        CheckKeyPush(Config.KeyInput_ENUM.Keyboard, keyCode);
                    }
                    else if (this.KeyStates[keyCode] < 0 && KS >= 0)
                    {
                        CheckKeyRelease(Config.KeyInput_ENUM.Keyboard, keyCode);
                    }

                    this.KeyStates[keyCode] = KS;
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                ExceptionMessage(ex, "[CheckKeyState]", true);
#endif
                Log.Debug("[CheckKeyState] (Error) " + ex.Message);
            }
        }
        #endregion

        #region - 外部入力
        private void CheckKeyState2(object sender, UdpComRcvMessageEventArgs e)
        {
            try
            {
                var xml = XDocument.Parse(e.Message);
                if (xml != null)
                {
                    // xmlを解析
                    var table = xml.Element("PluginManager");

                    if (table != null)
                    {
                        // KeyDown
                        var rows = table.Elements("KeyDown");
                        if (rows != null)
                        {
                            foreach (XElement row2 in rows)
                            {
                                int keyCode;
                                if (int.TryParse(row2.Value, out keyCode) == true)
                                {
                                    CheckKeyPush(Config.KeyInput_ENUM.Com, keyCode);
                                }
                            }
                        }

                        // KeyUp
                        rows = table.Elements("KeyUp");
                        if (rows != null)
                        {
                            foreach (XElement row2 in rows)
                            {
                                int keyCode;
                                if (int.TryParse(row2.Value, out keyCode) == true)
                                {
                                    CheckKeyRelease(Config.KeyInput_ENUM.Com, keyCode);
                                }
                            }
                        }

                    }

                    // 各プラグインへ (未実装)

                }
            }
            catch (Exception ex)
            {
#if DEBUG
                ExceptionMessage(ex, "[CheckKeyState2]", true);
#endif
                Log.Error("[CheckKeyState2] " + ex.Message, ex);
            }
        }
        #endregion

        #region キーボード押
        private void CheckKeyPush(Config.KeyInput_ENUM _KeyInput, int _KeyCode)
        {
            try
            {
                foreach(DllStruct dllStruct in this.Dlls)
                {
                    if (dllStruct.handle != null && dllStruct.handle != IntPtr.Zero)
                    {
                        foreach(Config.CFG_DLL.KeyControl keyControl in dllStruct.CFG.keyControl)
                        {
                            if (keyControl.KeyInput == _KeyInput && keyControl.KeyCode == _KeyCode)
                            {
                                if (keyControl.IsExclusive == true)
                                {
#if DEBUG
                                    Log.Debug("[CheckKeyPush] 一致(排他) : " + System.IO.Path.GetFileName(dllStruct.CFG.DllPath) + " " + (!dllStruct.NFB).ToString());
#endif
                                    if (dllStruct.NFB == true)
                                    {
                                        dllStruct.NFB = false;
                                    }
                                    else
                                    {
                                        // 他のNFBをOFFにする(いったん全部のNFBをOFFにする)
                                        foreach (DllStruct dllStruct2 in this.Dlls)
                                        {
                                            dllStruct2.NFB = false;
                                        }
                                        dllStruct.NFB = true;
                                    }
                                }
                                else
                                {
#if DEBUG
                                    Log.Debug("[CheckKeyPush] 一致(非排他) : " + System.IO.Path.GetFileName(dllStruct.CFG.DllPath) + " " + (!dllStruct.NFB).ToString());
#endif
                                    dllStruct.NFB = !dllStruct.NFB;
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
#if DEBUG
                ExceptionMessage(ex, "[CheckKeyPush]", true);
#endif
                Log.Error("[CheckKeyPush] " + ex.Message, ex);
            }
        }
        #endregion

        #region キーボード離
        private void CheckKeyRelease(Config.KeyInput_ENUM _KeyInput, int _KeyCode)
        {
            try
            {
                foreach (DllStruct dllStruct in this.Dlls)
                {
                    if (dllStruct.handle != null && dllStruct.handle != IntPtr.Zero)
                    {
                        foreach (Config.CFG_DLL.KeyControl keyControl in dllStruct.CFG.keyControl)
                        {
                            if (keyControl.KeyInput == _KeyInput && keyControl.KeyCode == _KeyCode)
                            {
                                // なにもしない
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                ExceptionMessage(ex, "[CheckKeyRelease]", true);
#endif
                Log.Error("[CheckKeyRelease] " + ex.Message,ex);
            }
        }
        #endregion

        #endregion

        #region SetHornBlow
        public void HornBlow(int hornIndex)
        {
            try
            {
#if DEBUG
                Log.Debug("[HornBlow] - Start : " + hornIndex.ToString());
#endif
                // 各DLL処理
                if (this.IsDisposing == false && this.Dlls != null && this.Dlls.Count > 0)
                {
                    foreach (DllStruct dllstruct in this.Dlls)
                    {
                        if (dllstruct.handle != IntPtr.Zero && dllstruct.hornBlowDelegate != null)
                        {
                            try
                            {
                                dllstruct.hornBlowDelegate(hornIndex);
                            }
                            catch (Exception ex2)
                            {
                                ExceptionMessage(ex2, "[MainHornBlow]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + ">", true);
                            }
                        }
                    }
                }
#if DEBUG
                Log.Debug("[HornBlow] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainHornBlow]", true);
            }
        }
        #endregion

        #region DoorOpen
        public void DoorOpen()
        {
            try
            {
#if DEBUG
                Log.Debug("[DoorOpen] - Start");
#endif
                // 各DLL処理
                if (this.IsDisposing == false && this.Dlls != null && this.Dlls.Count > 0)
                {
                    foreach (DllStruct dllstruct in this.Dlls)
                    {
                        if (dllstruct.handle != IntPtr.Zero && dllstruct.doorOpenDelegate != null)
                        {
                            try
                            {
                                dllstruct.doorOpenDelegate();
                            }
                            catch (Exception ex2)
                            {
                                ExceptionMessage(ex2, "[MainDoorOpen]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + ">", true);
                            }
                        }
                    }
                }
#if DEBUG
                Log.Debug("[DoorOpen] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainDoorOpen]", true);
            }
        }
        #endregion

        #region DoorClose
        public void DoorClose()
        {
            try
            {
#if DEBUG
                Log.Debug("[DoorClose] - Start");
#endif
                // 各DLL処理
                if (this.IsDisposing == false && this.Dlls != null && this.Dlls.Count > 0)
                {
                    foreach (DllStruct dllstruct in this.Dlls)
                    {
                        if (dllstruct.handle != IntPtr.Zero && dllstruct.doorCloseDelegate != null)
                        {
                            try
                            {
                                dllstruct.doorCloseDelegate();
                            }
                            catch (Exception ex2)
                            {
                                ExceptionMessage(ex2, "[MainDoorClose]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + ">", true);
                            }
                        }
                    }
                }
#if DEBUG
                Log.Debug("[DoorClose] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainDoorClose]", true);
            }
        }
        #endregion

        #region SetSignal
        public void SetSignal(int signalIndex)
        {
            try
            {
#if DEBUG
                Log.Debug("[SetSignal] - Start : " + signalIndex.ToString());
#endif
#if METRO2PT
                signalIndex = EngineUnique.SetSignal(signalIndex);
#if DEBUG
                Log.Debug("[SetSignal] - Conv : " + signalIndex.ToString());
#endif
                if (signalIndex < 0) return;
                // 各DLL処理
                if (this.IsDisposing == false && this.Dlls != null && this.Dlls.Count > 0)
                {
                    foreach (DllStruct dllstruct in this.Dlls)
                    {
                        if (dllstruct.handle != IntPtr.Zero && dllstruct.setSignalDelegate != null)
                        {
                            try
                            {
                                dllstruct.setSignalDelegate(signalIndex);
                            }
                            catch (Exception ex2)
                            {
                                ExceptionMessage(ex2, "[MainSetSignal]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + ">", true);
                            }
                        }
                    }
                }

#else
                int nSignalIndex = signalIndex;
                if (this.CFG.General.SignalScript != null && this.CFG.General.SignalScript.Count > 0 && this.CFG.General.SignalScript.ContainsKey(signalIndex) == true)
                {
                    try
                    {
                        Log.Debug("[SetSignal] - 解析 : InSig=" + signalIndex.ToString());

                        // 初期化
                        this.Variables = new Dictionary<string, string>();

                        // 予約語代入
                        if (Variables.ContainsKey("$InSig")) Variables["$InSig"] = signalIndex.ToString();
                        else Variables.Add("$InSig", signalIndex.ToString());

                        if (Variables.ContainsKey("$OutSig")) Variables["$OutSig"] = signalIndex.ToString();
                        else Variables.Add("$OutSig", signalIndex.ToString());


                        // 改行と ; で区切る
                        for (int i = 0; i < this.CFG.General.SignalScript[signalIndex].Count(); i++)
                        {
                            string[] vars = this.CFG.General.SignalScript[signalIndex][i].Split(new char[] { '\n', ';' });
                            foreach (string value in vars)
                            {
#if DEBUG
                            Log.Debug("[SetSignal] - 解析中 : " + value);
#endif
                                if (value.IndexOf("=") != -1)
                                {
                                    // =がある -> 代入式とみなす && 距離程行ではない
                                    string[] vals2 = value.Split('=');
                                    // 解析
                                    double z = 0;
                                    try
                                    {
                                        z = HL.CalcOpe.Program.Main(vals2[1], this.pMyVeshicleState.NewState.Location, this.Variables);
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Error("[SetSignal] - 解析エラー : " + vals2[1] + "; " + ex.Message);
                                    }
                                    string v = vals2[0].Trim(' ').Trim('\t');
                                    if (Variables.ContainsKey(v)) Variables[v] = z.ToString();
                                    else Variables.Add(v, z.ToString());
#if DEBUG
                                Log.Debug("[SetSignal] - 解析結果 : " + v + " = " + z.ToString());
#endif
                                }

                            }
                            nSignalIndex = HL.StrOpe.ToInt(Variables["$OutSig"]);
                            Log.Debug("[SetSignal] - 解析終了(" + i.ToString() + ") : Sig=" + nSignalIndex.ToString());

                            // 各DLL処理
                            if (this.IsDisposing == false && this.Dlls != null && this.Dlls.Count > 0 && nSignalIndex >= 0)
                            {
                                foreach (DllStruct dllstruct in this.Dlls)
                                {
                                    if (dllstruct.handle != IntPtr.Zero && dllstruct.setSignalDelegate != null)
                                    {
                                        try
                                        {
                                            dllstruct.setSignalDelegate(nSignalIndex);
                                        }
                                        catch (Exception ex3)
                                        {
                                            ExceptionMessage(ex3, "[MainSetSignal]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + ">", true);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex2)
                    {
                        Log.Error("[SetSignal] - Error2 : " + ex2.Message, ex2);
                        nSignalIndex = signalIndex;
                    }
                }
                else
                {
                    Log.Debug("[SetSignal] - 解析対象外 : InSig=" + signalIndex.ToString());

                    // 各DLL処理
                    if (this.IsDisposing == false && this.Dlls != null && this.Dlls.Count > 0 && nSignalIndex >= 0)
                    {
                        foreach (DllStruct dllstruct in this.Dlls)
                        {
                            if (dllstruct.handle != IntPtr.Zero && dllstruct.setSignalDelegate != null)
                            {
                                dllstruct.setSignalDelegate(nSignalIndex);
                            }
                        }
                    }
                }

#endif
#if DEBUG
                Log.Debug("[SetSignal] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetSignal]", true);
            }
        }
        #endregion

        #region SetBeaconData
#if SC
        /// <summary>
        /// SC
        /// </summary>
        /// <param name="beaconData"></param>
        public void SetBeaconData(AtsDefine.AtsBeaconData beaconData)
        {
            try
            {
#if DEBUG
                Log.Debug("[SetBeaconData] - Start");
#endif
                // 各DLL処理
                if (this.IsDisposing == false && this.Dlls != null && this.Dlls.Count > 0)
                {
                    foreach (DllStruct dllstruct in this.Dlls)
                    {
                        if (dllstruct.handle != IntPtr.Zero && dllstruct.setBeaconDataDelegate != null)
                        {
                            if (dllstruct.CFG.BeaconScript.Count > 0)
                            {
                                if (dllstruct.CFG.BeaconScript.ContainsKey(beaconData.Type) == true)
                                {
                                    AtsMain.AtsBeaconData nBeaconData = Script.Run(dllstruct.CFG.BeaconScript[beaconData.Type], beaconData);
                                    if (nBeaconData.Type >= 0)
                                    {
                                        dllstruct.setBeaconDataDelegate(nBeaconData);
                                    }
#if DEBUG
                                    Log.Write("[SetBeaconData] - Before(" + beaconData.Type.ToString() + ", " + beaconData.Signal.ToString() + ", " + beaconData.Distance.ToString() + ", " + beaconData.Optional.ToString() + ") ");
#endif
                                }
                            }
                            else
                            {
                                // そのまま
                                dllstruct.setBeaconDataDelegate(beaconData);
                            }
                        }
                    }
                }
#if DEBUG
                Log.Write("[SetBeaconData] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetBeaconData]", true);
            }
        }

#elif SWP || METRO2PT
        /// <summary>
        /// SWP2PT
        /// </summary>
        /// <param name="beaconData"></param>
        public void SetBeaconData(AtsDefine.AtsBeaconData beaconData)
        {
            try
            {
#if DEBUG
                Log.Write("[SetBeaconData] - Start : " + beaconData.Type.ToString() + ", " + beaconData.Signal.ToString() + "," + beaconData.Distance.ToString("0") + ", " + beaconData.Optional.ToString());
#endif
                // 各DLL処理
                if (this.IsDisposing == false && this.Dlls != null && this.Dlls.Count > 0)
                {
                    // 先処理
                    AtsDefine.AtsBeaconData nBeaconData;
                    nBeaconData = beaconData;

#if SWP
                    switch (beaconData.Type)
                    {
                        case 16:
                            if (EngineUnique.GetLimitSpeed(0) < 999)
                            {
                                // いったん制限解除
                                nBeaconData.Type = 313019;
                                SetBeaconData(nBeaconData);
                            }
                            break;
                        case 18:
                            if (EngineUnique.GetLimitSpeed(1) < 999)
                            {
                                // いったん制限解除
                                nBeaconData.Type = 313019;
                                SetBeaconData(nBeaconData);
                            }
                            break;
                        case 19:
                            if (EngineUnique.GetLimitSpeed(2) < 999)
                            {
                                // いったん制限解除
                                nBeaconData.Type = 313019;
                                SetBeaconData(nBeaconData);
                            }
                            break;
                        case 20:
                            if (EngineUnique.GetLimitSpeed(3) < 999)
                            {
                                // いったん制限解除
                                nBeaconData.Type = 313019;
                                SetBeaconData(nBeaconData);
                            }
                            break;
                        default:
                            break;
                    }
                    // 変換
                    nBeaconData = EngineUnique.SetBeaconData(beaconData, this.pMyVeshicleState);
#if DEBUG
                    Log.Write("[SetBeaconData] - Conv : " + nBeaconData.Type.ToString() + ", " + nBeaconData.Signal.ToString() + "," + nBeaconData.Distance.ToString("0") + ", " + nBeaconData.Optional.ToString());
#endif
                    if (nBeaconData.Type < 0) return;

#elif METRO2PT
                    // 変換
                    nBeaconData = EngineUnique.SetBeaconData(beaconData);
#if DEBUG
                    Log.Write("[SetBeaconData] - Conv : " + nBeaconData.Type.ToString() + ", " + nBeaconData.Signal.ToString() + "," + nBeaconData.Distance.ToString("0") + ", " + nBeaconData.Optional.ToString());
#endif
                    if (nBeaconData.Type < 0) return;

#endif

                    foreach (DllStruct dllstruct in this.Dlls)
                    {
                        if (dllstruct.handle != IntPtr.Zero && dllstruct.setBeaconDataDelegate != null)
                        {
                            dllstruct.setBeaconDataDelegate(nBeaconData);
                        }
                    }
                }
#if DEBUG
                Log.Write("[SetBeaconData] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetBeaconData]", true);
            }
        }
#elif SWPX
        double pBeaconDistance = 0;
        int pBeaconCount = 0;
        float pSectionDistance = 0;
        float[] LimitDistance = new float[4];
        int[] LimitSpeed = new int[4];

        /// <summary>
        /// SWP2PT
        /// </summary>
        /// <param name="beaconData"></param>
        public void SetBeaconData(AtsDefine.AtsBeaconData beaconData)
        {
            try
            {
#if DEBUG
                Log.Write("[SetBeaconData] - Start");
#endif
                // 各DLL処理
                if (this.IsDisposing == false && this.Dlls != null && this.Dlls.Count > 0)
                {
                    foreach (DllStruct dllstruct in this.Dlls)
                    {
                        if (dllstruct.handle != IntPtr.Zero && dllstruct.setBeaconDataDelegate != null)
                        {
                            // SWP2
                            /*
                            [3] PT閉塞地上子
                                保持している地上子距離程と異なった場合はリセット
                                信号インデックスが0かつ保持しているOptionalより小さな値が入ってきた場合は保持するOptinalとDistanceを更新
                                Optionalが9以上の場合は信号インデックスが0であるとみなす
                                保持しているデータをPTプラグインに渡す
                                [PT60]に変換して渡す
                            [4][5] PT即時停止地上子
                                信号インデックスが0なら即時停止
                                5は無閉塞を許可する
                                [PT60]に変換して渡す
                            [6][9][10] PT速度制限
                            [8] PT下り勾配速度制限
                                Optionalの下３桁が制限速度、上が制限区間までの距離
                                [PT70]に変換して渡す
                            [16][18][19][20] PT速度制限区間終了
                                [PT70]に制限開始距離4・制限区間長4として渡す
                            [25] PE地上子
                                0:[PT90]にそのまま渡す
                                1:[PT68]
                                2:[PT69]
                            [0] STロング
                                [PT0]にそのまま渡す
                            [1] ST直下
                                [PT1]にそのまま渡す
                            [55] ST速度照査
                                Optionalに指定速度
                                上回っていた場合は即時停止
                                [PT10]に速度指定で渡す
                            [56] ST速度照査
                                Optionalに指定速度
                                上回っていた場合は即時停止
                                [PT10]に速度指定で渡す
                            [203]勾配(‰)
                                */
                            AtsMain.AtsBeaconData nBeaconData;
                            nBeaconData = beaconData;
                            nBeaconData.Type = -1;

                            switch (beaconData.Type)
                            {
                                case 0:
                                    // ATS-S ロング
                                    nBeaconData.Type = 0;
                                    break;

                                case 1:
                                    // ATS-S 即時停止
                                    nBeaconData.Type = 0;
                                    break;

                                case 3: // PT TL・TR地上子
                                    if (beaconData.Optional < 9)
                                    {
                                        if (this.pBeaconDistance != this.pMyVeshicleState.Location)
                                        {
                                            this.pBeaconDistance = this.pMyVeshicleState.Location;
                                            this.pBeaconCount = -1;
                                            this.pSectionDistance = 0;
                                        }
                                        if (this.pBeaconCount < 0 || (this.pBeaconCount < beaconData.Optional && ((beaconData.Distance < this.pSectionDistance) || (this.pSectionDistance == 0))))
                                        {
                                            if (beaconData.Signal == 0)
                                            {
                                                this.pBeaconCount = 10;
                                                this.pSectionDistance = beaconData.Distance - 10;

                                                nBeaconData.Type = 60;
                                                nBeaconData.Signal = 4;
                                                nBeaconData.Distance = (float)(this.pSectionDistance);
                                                if (pSectionDistance > 3996)
                                                {
                                                    nBeaconData.Optional = 1999020020; // 無閉塞有効
                                                }
                                                else
                                                {
                                                    nBeaconData.Optional = (int)(pSectionDistance / 4) * 1000000 + 1000000000 + 20020; // 無閉塞有効
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if ((this.pBeaconDistance != this.pMyVeshicleState.Location) || (this.pBeaconCount < 0))
                                        {
                                            this.pBeaconCount = 9;
                                            this.pSectionDistance = beaconData.Distance - 10;
                                        }
                                        //Optional = 0 xxx 020 020の形に変換して60番地上子に送る
                                        if (this.pSectionDistance <= 0) this.pSectionDistance = 4;
                                        if (this.pSectionDistance > 4000) this.pSectionDistance = 3999;
                                        nBeaconData.Type = 60;
                                        nBeaconData.Signal = 4;
                                        nBeaconData.Distance = (float)(this.pSectionDistance);
                                        if (pSectionDistance > 3996)
                                        {
                                            nBeaconData.Optional = 1999020020; // 無閉塞有効
                                        }
                                        else
                                        {
                                            nBeaconData.Optional = (int)(pSectionDistance / 4) * 1000000 + 1000000000 + 20020; // 無閉塞有効
                                        }
                                    }
                                    break;

                                case 4: // PT TS地上子 (即停)
                                    if (beaconData.Signal == 0 && beaconData.Distance < 50)
                                    {
                                        nBeaconData.Type = 60;
                                        nBeaconData.Signal = 0;
                                        nBeaconData.Distance = beaconData.Distance;
                                        nBeaconData.Optional = (int)(beaconData.Distance / 4) * 1000000 + 20020; // 即時停止有効
                                    }
                                    break;

                                case 5: // PT TM地上子
                                    if (beaconData.Signal == 0 && beaconData.Distance < 50)
                                    {
                                        nBeaconData.Type = 60;
                                        nBeaconData.Signal = 0;
                                        nBeaconData.Distance = beaconData.Distance;
                                        nBeaconData.Optional = (int)(beaconData.Distance / 4) * 1000000 + 1000000000 + 20020; // 無閉塞有効
                                    }
                                    break;

                                case 6: // PT 速度制限
                                    LimitDistance[0] = (float)((int)(beaconData.Optional / 1000));
                                    LimitSpeed[0] = beaconData.Optional % 1000;
                                    SetLimitBeacon(beaconData);
                                    break;
                                case 8: // PT 下り勾配速度制限
                                    LimitDistance[1] = (float)((int)(beaconData.Optional / 1000));
                                    LimitSpeed[1] = beaconData.Optional % 1000;
                                    SetLimitBeacon(beaconData);
                                    break;
                                case 9: // PT 分岐器速度制限
                                    LimitDistance[2] = (float)((int)(beaconData.Optional / 1000));
                                    LimitSpeed[2] = beaconData.Optional % 1000;
                                    SetLimitBeacon(beaconData);
                                    break;
                                case 10: // PT 臨時速度制限
                                    LimitDistance[3] = (float)((int)(beaconData.Optional / 1000));
                                    LimitSpeed[3] = beaconData.Optional % 1000;
                                    SetLimitBeacon(beaconData);
                                    /*
                                                            beaconData.Type = 70;
                                                            beaconData.Signal = 4;
                                                            beaconData.Distance = sectionDistance;
                                                            beaconData.Optional = (int)((beaconData.Optional % 1000) / 5) * 1000000 + (int)(beaconData.Optional / 4000) * 1000 + 999;
                                                            PTSetBeaconData(beaconData);
                                    */
                                    break;
                                case 16: // PT 速度制限解除
                                    if (LimitSpeed[0] < 999)
                                    {
                                        // いったん制限解除
                                        nBeaconData.Type = 313019;
                                        dllstruct.setBeaconDataDelegate(nBeaconData);

                                        LimitDistance[0] = 0;
                                        LimitSpeed[0] = 999;
                                        nBeaconData = SetLimitBeacon(beaconData);
                                    }
                                    break;
                                case 18: // PT 下り勾配速度制限解除
                                    if (LimitSpeed[1] < 999)
                                    {
                                        // いったん制限解除
                                        nBeaconData.Type = 313019;
                                        dllstruct.setBeaconDataDelegate(nBeaconData);

                                        LimitDistance[1] = 0;
                                        LimitSpeed[1] = 999;
                                        nBeaconData = SetLimitBeacon(beaconData);
                                    }
                                    break;
                                case 19: // PT 分岐器速度制限解除
                                    if (LimitSpeed[2] < 999)
                                    {
                                        // いったん制限解除
                                        nBeaconData.Type = 313019;
                                        dllstruct.setBeaconDataDelegate(nBeaconData);

                                        LimitDistance[2] = 0;
                                        LimitSpeed[2] = 999;
                                        nBeaconData = SetLimitBeacon(beaconData);
                                    }
                                    break;
                                case 20: // PT 臨時速度制限解除
                                    if (LimitSpeed[3] < 999)
                                    {
                                        // いったん制限解除
                                        nBeaconData.Type = 313019;
                                        dllstruct.setBeaconDataDelegate(nBeaconData);

                                        LimitDistance[3] = 0;
                                        LimitSpeed[3] = 999;
                                        nBeaconData = SetLimitBeacon(beaconData);
                                    }
                                    /*
                                                            beaconData.Type = 70;
                                                            beaconData.Signal = 4;
                                                            beaconData.Distance = sectionDistance;
                                                            beaconData.Optional = 999000001 + ((g_cars * 5) % 1000) * 1000; // 1両20m換算
                                                            PTSetBeaconData(beaconData);
                                    */
                                    break;

                                case 25: // PE地上子
                                    if (beaconData.Optional == 0)
                                    {
                                        // PE地上子
                                        nBeaconData.Type = 90;
                                    }
                                    else if (beaconData.Optional == 1)
                                    {
                                        // KE地上子 (拠点PでPを復活)
                                        //beaconData.Type = 69;
                                        //PTSetBeaconData(beaconData);
                                    }
                                    else if (beaconData.Optional == 2)
                                    {
                                        // PE地上子 (拠点PでPを休止)
                                        nBeaconData.Type = 90;
                                    }
                                    break;

                                case 55: // ST速度制限
                                case 56: // ST速度制限
                                    if (this.pMyVeshicleState.NewState.Speed > 0)
                                    {
                                        nBeaconData.Type = 313010;
                                        if (beaconData.Optional > 0)
                                        {
                                            nBeaconData.Optional = beaconData.Optional * 10;
                                        }
                                        else
                                        {
                                            // 強制停止
                                            nBeaconData.Optional = -1;
                                        }
                                    }
                                    break;

                                case 203:
                                    // 勾配設定
                                    nBeaconData.Type = 75;
                                    break;

                                default:
                                    break;
                            }

                            if (nBeaconData.Type >= 0)
                            {
                                dllstruct.setBeaconDataDelegate(nBeaconData);
                            }
                        }
                    }
                }
#if DEBUG
                Log.Write("[SetBeaconData] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetBeaconData]", true);
            }
        }
        private AtsMain.AtsBeaconData SetLimitBeacon(AtsMain.AtsBeaconData beaconData)
        {
            AtsMain.AtsBeaconData pBeaconData = beaconData;
            pBeaconData.Type = -1;

            int LS = 999;
            float LD = 0;
            int i = 0;
            for (i = 0; i < 4; i++)
            {
                if (LS > LimitSpeed[i] && LimitDistance[i] > 0)
                {
                    LS = LimitSpeed[i];
                    LD = LimitDistance[i];
                    if (LD >= 4000)
                    {
                        LD = 3999;
                    }
                }
            }

            // いったん制限解除
            //this.AtsPT.ResetLimitPattern();

            if (LS < 999)
            {
                // 制限
                pBeaconData.Type = 70;
                pBeaconData.Signal = 4;
                pBeaconData.Distance = (float)(this.pSectionDistance);
                pBeaconData.Optional = (int)(LS / 5) * 1000000 + (int)(LD / 4) * 1000 + 1999;
            }

            return pBeaconData;
        }


#else
        /// <summary>
        /// 標準版
        /// </summary>
        /// <param name="beaconData"></param>
        public void SetBeaconData(AtsDefine.AtsBeaconData beaconData)
        {
            try
            {
#if DEBUG
                Log.Debug("[SetBeaconData] - Start");
#endif
                AtsDefine.AtsBeaconData nBeaconData = beaconData;
                if (this.CFG.General.BeaconScript != null && this.CFG.General.BeaconScript.Count > 0 && this.CFG.General.BeaconScript.ContainsKey(beaconData.Type) == true)
                {
                    try
                    {
                        Log.Debug("[SetBeaconData] - 解析 : Loc=" + this.pMyVeshicleState.NewState.Location.ToString("0.000") + ", InType=" + beaconData.Type.ToString() + ", InLen=" + beaconData.Distance.ToString() + ", InSig=" + beaconData.Signal.ToString() + ", InData=" + beaconData.Optional.ToString());

                        // 初期化
                        this.Variables = new Dictionary<string, string>();

                        // 予約語代入
                        if (Variables.ContainsKey("$InType")) Variables["$InType"] = beaconData.Type.ToString();
                        else Variables.Add("$InType", beaconData.Type.ToString());

                        if (Variables.ContainsKey("$InSig")) Variables["$InSig"] = beaconData.Signal.ToString();
                        else Variables.Add("$InSig", beaconData.Signal.ToString());

                        if (Variables.ContainsKey("$InLen")) Variables["$InLen"] = beaconData.Distance.ToString();
                        else Variables.Add("$InLen", beaconData.Distance.ToString());

                        if (Variables.ContainsKey("$InData")) Variables["$InData"] = beaconData.Optional.ToString();
                        else Variables.Add("$InData", beaconData.Optional.ToString());

                        if (Variables.ContainsKey("$OutType")) Variables["$OutType"] = beaconData.Type.ToString();
                        else Variables.Add("$OutType", beaconData.Type.ToString());

                        if (Variables.ContainsKey("$OutSig")) Variables["$OutSig"] = beaconData.Signal.ToString();
                        else Variables.Add("$OutSig", beaconData.Signal.ToString());

                        if (Variables.ContainsKey("$OutLen")) Variables["$OutLen"] = beaconData.Distance.ToString();
                        else Variables.Add("$OutLen", beaconData.Distance.ToString());

                        if (Variables.ContainsKey("$OutData")) Variables["$OutData"] = beaconData.Optional.ToString();
                        else Variables.Add("$OutData", beaconData.Optional.ToString());


                        // 改行と ; で区切る
                        for(int i=0; i < this.CFG.General.BeaconScript[beaconData.Type].Count(); i++)
                        {
                            string[] vars = this.CFG.General.BeaconScript[beaconData.Type][i].Split(new char[] { '\n', ';' });
                            foreach (string value in vars)
                            {
#if DEBUG
                            Log.Debug("[SetBeaconData] - 解析中 : " + value);
#endif
                                if (value.IndexOf("=") != -1)
                                {
                                    // =がある -> 代入式とみなす && 距離程行ではない
                                    string[] vals2 = value.Split('=');
                                    // 解析
                                    double z = 0;
                                    try
                                    {
                                        z = HL.CalcOpe.Program.Main(vals2[1], this.pMyVeshicleState.NewState.Location, this.Variables);
                                    }
                                    catch (Exception ex)
                                    {
                                        Log.Error("[SetBeaconData] - 解析エラー : " + vals2[1] + "; " + ex.Message, ex);
                                    }
                                    string v = vals2[0].Trim(' ').Trim('\t');
                                    if (Variables.ContainsKey(v)) Variables[v] = z.ToString();
                                    else Variables.Add(v, z.ToString());
#if DEBUG
                                Log.Debug("[SetBeaconData] - 解析結果 : " + v + " = " + z.ToString());
#endif
                                }

                            }
                            nBeaconData.Type = HL.StrOpe.ToInt(Variables["$OutType"]);
                            nBeaconData.Signal = HL.StrOpe.ToInt(Variables["$OutSig"]);
                            nBeaconData.Distance = (float)HL.StrOpe.ToDbl(Variables["$OutLen"]);
                            nBeaconData.Optional = HL.StrOpe.ToInt(Variables["$OutData"]);
                            if(Variables.ContainsKey("$PutLoc") == true)
                            {
                                // 後で発火
                                LocationBeaconDt dt = new LocationBeaconDt();
                                dt.Location = HL.StrOpe.ToDbl(Variables["$PutLoc"]);
                                dt.BeaconData = nBeaconData;
                                this.lLocBcn.Add(dt);
                                Variables.Remove("$PutLoc"); // 今回限りの変数なので削除
                                Log.Debug("[SetBeaconData] - 実行予約(" + i.ToString() + ") : PutLoc=" + dt.Location.ToString("0.000") + ", Type=" + nBeaconData.Type.ToString() + ", Len=" + nBeaconData.Distance.ToString() + ", Sig=" + nBeaconData.Signal.ToString() + ", Data=" + nBeaconData.Optional.ToString());
                                // 今回は発火しないので-1
                                nBeaconData.Type = -1;
                            }
                            else
                            {
                                Log.Debug("[SetBeaconData] - 解析終了(" + i.ToString() + ") : Type=" + nBeaconData.Type.ToString() + ", Len=" + nBeaconData.Distance.ToString() + ", Sig=" + nBeaconData.Signal.ToString() + ", Data=" + nBeaconData.Optional.ToString());
                            }

                            // 各DLL処理
                            if (this.IsDisposing == false && this.Dlls != null && this.Dlls.Count > 0 && nBeaconData.Type >= 0)
                            {
                                foreach (DllStruct dllstruct in this.Dlls)
                                {
                                    if (dllstruct.handle != IntPtr.Zero && dllstruct.setBeaconDataDelegate != null)
                                    {
                                        try
                                        {
                                            dllstruct.setBeaconDataDelegate(nBeaconData);
                                        }
                                        catch (Exception ex3)
                                        {
                                            ExceptionMessage(ex3, "[MainSetBeaconData]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + ">", true);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex2)
                    {
                        Log.Error("[SetBeaconData] - Error2 : " + ex2.Message, ex2);
                        nBeaconData = beaconData;
                    }
                }
                else
                {
                    Log.Debug("[SetBeaconData] - 解析対象外 : Loc=" + this.pMyVeshicleState.NewState.Location.ToString("0.000") + ", InType=" + beaconData.Type.ToString() + ", InLen=" + beaconData.Distance.ToString() + ", InSig=" + beaconData.Signal.ToString() + ", InData=" + beaconData.Optional.ToString());

                    // 各DLL処理
                    if (this.IsDisposing == false && this.Dlls != null && this.Dlls.Count > 0 && nBeaconData.Type >= 0)
                    {
                        foreach (DllStruct dllstruct in this.Dlls)
                        {
                            if (dllstruct.handle != IntPtr.Zero && dllstruct.setBeaconDataDelegate != null)
                            {
                                dllstruct.setBeaconDataDelegate(nBeaconData);
                            }
                        }
                    }
                }

#if DEBUG
                Log.Debug("[SetBeaconData] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetBeaconData]", true);
            }
        }
#endif
#endregion


        #region SetHandles
        public void SetHandles(int powerHandle, int brakeHandle, int reverserHandle, int cscHandle)
        {
            try
            {
#if DEBUG
                Log.Debug("[SetHandles] - Start");
#endif
                // 各DLL処理
                if (this.IsDisposing == false && this.Dlls != null && this.Dlls.Count > 0)
                {
                    for (int i = 0; i < this.Dlls.Count; i++)
                    {
                        SetHandles(i, powerHandle, brakeHandle, reverserHandle, cscHandle);
                    }
                }
#if DEBUG
                Log.Debug("[SetHandles] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetHandles]", true);
            }
        }
        public void SetHandles(int dllIndex, int powerHandle, int brakeHandle, int reverserHandle, int cscHandle)
        {
            try
            {
#if DEBUG
                Log.Debug("[SetHandles] - Start " + dllIndex.ToString());
#endif
                // 各DLL処理
                if (this.IsDisposing == false && this.Dlls != null && this.Dlls.Count > dllIndex)
                {
                    DllStruct dllstruct = this.Dlls[dllIndex];
                    if (dllstruct.handle != IntPtr.Zero && dllstruct.setHandleDelegate != null)
                    {
                        try
                        {
                            dllstruct.setHandleDelegate(powerHandle, brakeHandle, reverserHandle, cscHandle);
                        }
                        catch (Exception ex2)
                        {
                            ExceptionMessage(ex2, "[MainSetHandles]<" + System.IO.Path.GetFileName(dllstruct.CFG.DllPath) + ">", true);
                        }
#if DEBUG
                        Log.Debug("[SetHandles] - P=" + powerHandle.ToString() + ", B=" + brakeHandle.ToString() + ", R=" + reverserHandle.ToString() + ", C=" + cscHandle.ToString());
#endif
                    }
                }
#if DEBUG
                Log.Debug("[SetHandles] - End");
#endif
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "[MainSetHandles]", true);
            }
        }
#endregion
    }
}
