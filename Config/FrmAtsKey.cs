﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConfigTool
{
    public class FrmAtsKey : Form
    {
        int rKeyCode = 0;

        #region コントロール定義
        private Label lblKeyCodeTitle;
        private Label lblKeyCode;
        private Label lblKeyValueTitle;
        private Label lblKeyValue;
        private Button btnCancel;
        private Button btnOK;
        private Label lblCaption;
        #endregion

        #region イニシャライズ
        private void InitializeComponent()
        {
            this.lblCaption = new System.Windows.Forms.Label();
            this.lblKeyCodeTitle = new System.Windows.Forms.Label();
            this.lblKeyCode = new System.Windows.Forms.Label();
            this.lblKeyValueTitle = new System.Windows.Forms.Label();
            this.lblKeyValue = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblCaption
            // 
            this.lblCaption.AutoSize = true;
            this.lblCaption.Location = new System.Drawing.Point(13, 9);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(216, 17);
            this.lblCaption.TabIndex = 0;
            this.lblCaption.Text = "割り当てたいキーを押してください";
            // 
            // lblKeyCodeTitle
            // 
            this.lblKeyCodeTitle.AutoSize = true;
            this.lblKeyCodeTitle.Location = new System.Drawing.Point(70, 79);
            this.lblKeyCodeTitle.Name = "lblKeyCodeTitle";
            this.lblKeyCodeTitle.Size = new System.Drawing.Size(76, 17);
            this.lblKeyCodeTitle.TabIndex = 2;
            this.lblKeyCodeTitle.Text = "仮想ｷｰｺｰﾄﾞ";
            // 
            // lblKeyCode
            // 
            this.lblKeyCode.AutoSize = true;
            this.lblKeyCode.Location = new System.Drawing.Point(152, 79);
            this.lblKeyCode.Name = "lblKeyCode";
            this.lblKeyCode.Size = new System.Drawing.Size(15, 17);
            this.lblKeyCode.TabIndex = 3;
            this.lblKeyCode.Text = "0";
            // 
            // lblKeyValueTitle
            // 
            this.lblKeyValueTitle.AutoSize = true;
            this.lblKeyValueTitle.Location = new System.Drawing.Point(70, 101);
            this.lblKeyValueTitle.Name = "lblKeyValueTitle";
            this.lblKeyValueTitle.Size = new System.Drawing.Size(60, 17);
            this.lblKeyValueTitle.TabIndex = 4;
            this.lblKeyValueTitle.Text = "キー文字";
            // 
            // lblKeyValue
            // 
            this.lblKeyValue.AutoSize = true;
            this.lblKeyValue.Location = new System.Drawing.Point(152, 101);
            this.lblKeyValue.Name = "lblKeyValue";
            this.lblKeyValue.Size = new System.Drawing.Size(15, 17);
            this.lblKeyValue.TabIndex = 5;
            this.lblKeyValue.Text = "_";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(197, 226);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.TabStop = false;
            this.btnCancel.Text = "ｷｬﾝｾﾙ";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnOK.Location = new System.Drawing.Point(16, 226);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 7;
            this.btnOK.TabStop = false;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // frmAtsKey
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblKeyValue);
            this.Controls.Add(this.lblKeyValueTitle);
            this.Controls.Add(this.lblKeyCode);
            this.Controls.Add(this.lblKeyCodeTitle);
            this.Controls.Add(this.lblCaption);
            this.Font = new System.Drawing.Font("游ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.KeyPreview = true;
            this.Name = "frmAtsKey";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmAtsKey_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        #region コンストラクタ
        public FrmAtsKey()
        {
            InitializeComponent();
        }
        #endregion

        #region 呼び出し口
        public int Main(int keycode)
        {
            lblKeyCode.Text = keycode.ToString();
            lblKeyValue.Text = ((Keys)(keycode)).ToString();

            this.ShowDialog();

            return rKeyCode;
        }
        #endregion

        #region キーダウンイベント
        private void frmAtsKey_KeyDown(object sender, KeyEventArgs e) {
            //キーコードにより処理する
            lblKeyCode.Text = e.KeyCode.ToString();
            lblKeyValue.Text = e.KeyValue.ToString();
        }
        #endregion

        private void BtnPush_Click(object sender, EventArgs e)
        {

        }
        private void btnPush_KeyPress(object sender, KeyPressEventArgs e)
        {
            //lblKeyCode.Text = ((int)(e.KeyChar)).ToString();
            //lblKeyValue.Text = e.KeyChar.ToString();
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            rKeyCode = Convert.ToInt32(lblKeyValue.Text);
            this.Close();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
