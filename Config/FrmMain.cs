﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HCL;

using AtsPlugin;

namespace ConfigTool
{
    public class FrmMain : Form
    {
        #region メンバ変数
        private AtsPlugin.Config CFG;

        private string CfgFilePath;
        private int DispIndex;
        #endregion


        #region コントロール変数
        private Label label1;
        private TextBox txtFilePath;
        private Button btnSearchFile;
        private CheckBox chkInitialNFB;
        private CheckBox chkHasSetHandle;
        private GroupBox groupBox1;
        private CheckBox chkGetReverser;
        private CheckBox chkGetBrake;
        private CheckBox chkGetPower;
        private CheckBox chkGetHandles;
        private GroupBox groupBox2;
        private CheckBox chkSetReverser;
        private CheckBox chkSetBrake;
        private CheckBox chkSetPower;
        private CheckBox chkSetCsc;
        private ComboBox cboKeyInput;
        private Label label2;
        private Panel pnlKey;
        private Button btnAtsKey;
        private CheckBox chkExclusive;
        private TextBox txtKeyCode;
        private ComboBox cboAtsKey;
        private Label label4;
        private Label label3;
        private CheckBox chkNFB;
        private Button btnNew;
        private Button btnOpen;
        private Button btnSave;
        private Button btnUpd;
        private BindingSource bsDetail;
        private System.ComponentModel.IContainer components;
        private ToolTip toolTip1;
        private Button btnAddRow;
        private Button btnRemoveRow;
        private DataGridView dgvDetail;
        #endregion

        #region イニシャライズ
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFilePath = new System.Windows.Forms.TextBox();
            this.btnSearchFile = new System.Windows.Forms.Button();
            this.chkInitialNFB = new System.Windows.Forms.CheckBox();
            this.chkHasSetHandle = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkGetReverser = new System.Windows.Forms.CheckBox();
            this.chkGetBrake = new System.Windows.Forms.CheckBox();
            this.chkGetPower = new System.Windows.Forms.CheckBox();
            this.chkGetHandles = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkSetReverser = new System.Windows.Forms.CheckBox();
            this.chkSetBrake = new System.Windows.Forms.CheckBox();
            this.chkSetPower = new System.Windows.Forms.CheckBox();
            this.chkSetCsc = new System.Windows.Forms.CheckBox();
            this.dgvDetail = new System.Windows.Forms.DataGridView();
            this.cboKeyInput = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlKey = new System.Windows.Forms.Panel();
            this.btnAtsKey = new System.Windows.Forms.Button();
            this.chkExclusive = new System.Windows.Forms.CheckBox();
            this.txtKeyCode = new System.Windows.Forms.TextBox();
            this.cboAtsKey = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.chkNFB = new System.Windows.Forms.CheckBox();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnOpen = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnUpd = new System.Windows.Forms.Button();
            this.bsDetail = new System.Windows.Forms.BindingSource(this.components);
            this.btnAddRow = new System.Windows.Forms.Button();
            this.btnRemoveRow = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetail)).BeginInit();
            this.pnlKey.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(353, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "DLLファイル";
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(445, 72);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(251, 28);
            this.txtFilePath.TabIndex = 1;
            // 
            // btnSearchFile
            // 
            this.btnSearchFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearchFile.Location = new System.Drawing.Point(702, 72);
            this.btnSearchFile.Name = "btnSearchFile";
            this.btnSearchFile.Size = new System.Drawing.Size(60, 28);
            this.btnSearchFile.TabIndex = 2;
            this.btnSearchFile.Text = "参照";
            this.toolTip1.SetToolTip(this.btnSearchFile, "DLLファイルを探します");
            this.btnSearchFile.UseVisualStyleBackColor = true;
            this.btnSearchFile.Click += new System.EventHandler(this.btnSearchFile_Click);
            // 
            // chkInitialNFB
            // 
            this.chkInitialNFB.AutoSize = true;
            this.chkInitialNFB.Checked = true;
            this.chkInitialNFB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkInitialNFB.Location = new System.Drawing.Point(6, 8);
            this.chkInitialNFB.Name = "chkInitialNFB";
            this.chkInitialNFB.Size = new System.Drawing.Size(183, 21);
            this.chkInitialNFB.TabIndex = 4;
            this.chkInitialNFB.Text = "PluginConverter起動時ON";
            this.chkInitialNFB.UseVisualStyleBackColor = true;
            // 
            // chkHasSetHandle
            // 
            this.chkHasSetHandle.AutoSize = true;
            this.chkHasSetHandle.Enabled = false;
            this.chkHasSetHandle.Location = new System.Drawing.Point(445, 106);
            this.chkHasSetHandle.Name = "chkHasSetHandle";
            this.chkHasSetHandle.Size = new System.Drawing.Size(220, 21);
            this.chkHasSetHandle.TabIndex = 5;
            this.chkHasSetHandle.Text = "SetHandle関数を持っているDLL";
            this.chkHasSetHandle.UseVisualStyleBackColor = true;
            this.chkHasSetHandle.CheckedChanged += new System.EventHandler(this.ChkHasSetHandle_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkGetReverser);
            this.groupBox1.Controls.Add(this.chkGetBrake);
            this.groupBox1.Controls.Add(this.chkGetPower);
            this.groupBox1.Controls.Add(this.chkGetHandles);
            this.groupBox1.Location = new System.Drawing.Point(356, 147);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(404, 70);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "前のDLLから取得するハンドル値";
            // 
            // chkGetReverser
            // 
            this.chkGetReverser.AutoSize = true;
            this.chkGetReverser.Checked = true;
            this.chkGetReverser.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGetReverser.Location = new System.Drawing.Point(236, 27);
            this.chkGetReverser.Name = "chkGetReverser";
            this.chkGetReverser.Size = new System.Drawing.Size(80, 21);
            this.chkGetReverser.TabIndex = 8;
            this.chkGetReverser.Text = "Reverser";
            this.chkGetReverser.UseVisualStyleBackColor = true;
            // 
            // chkGetBrake
            // 
            this.chkGetBrake.AutoSize = true;
            this.chkGetBrake.Checked = true;
            this.chkGetBrake.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGetBrake.Location = new System.Drawing.Point(168, 27);
            this.chkGetBrake.Name = "chkGetBrake";
            this.chkGetBrake.Size = new System.Drawing.Size(62, 21);
            this.chkGetBrake.TabIndex = 7;
            this.chkGetBrake.Text = "Brake";
            this.chkGetBrake.UseVisualStyleBackColor = true;
            // 
            // chkGetPower
            // 
            this.chkGetPower.AutoSize = true;
            this.chkGetPower.Checked = true;
            this.chkGetPower.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkGetPower.Location = new System.Drawing.Point(97, 27);
            this.chkGetPower.Name = "chkGetPower";
            this.chkGetPower.Size = new System.Drawing.Size(65, 21);
            this.chkGetPower.TabIndex = 6;
            this.chkGetPower.Text = "Power";
            this.chkGetPower.UseVisualStyleBackColor = true;
            // 
            // chkGetHandles
            // 
            this.chkGetHandles.AutoSize = true;
            this.chkGetHandles.Enabled = false;
            this.chkGetHandles.Location = new System.Drawing.Point(6, 27);
            this.chkGetHandles.Name = "chkGetHandles";
            this.chkGetHandles.Size = new System.Drawing.Size(70, 21);
            this.chkGetHandles.TabIndex = 5;
            this.chkGetHandles.Text = "Handle";
            this.chkGetHandles.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkSetReverser);
            this.groupBox2.Controls.Add(this.chkSetBrake);
            this.groupBox2.Controls.Add(this.chkSetPower);
            this.groupBox2.Controls.Add(this.chkSetCsc);
            this.groupBox2.Location = new System.Drawing.Point(358, 223);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(404, 70);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "後続のDLLやBVEに渡すハンドル値";
            // 
            // chkSetReverser
            // 
            this.chkSetReverser.AutoSize = true;
            this.chkSetReverser.Checked = true;
            this.chkSetReverser.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSetReverser.Location = new System.Drawing.Point(236, 27);
            this.chkSetReverser.Name = "chkSetReverser";
            this.chkSetReverser.Size = new System.Drawing.Size(80, 21);
            this.chkSetReverser.TabIndex = 8;
            this.chkSetReverser.Text = "Reverser";
            this.chkSetReverser.UseVisualStyleBackColor = true;
            // 
            // chkSetBrake
            // 
            this.chkSetBrake.AutoSize = true;
            this.chkSetBrake.Checked = true;
            this.chkSetBrake.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSetBrake.Location = new System.Drawing.Point(168, 27);
            this.chkSetBrake.Name = "chkSetBrake";
            this.chkSetBrake.Size = new System.Drawing.Size(62, 21);
            this.chkSetBrake.TabIndex = 7;
            this.chkSetBrake.Text = "Brake";
            this.chkSetBrake.UseVisualStyleBackColor = true;
            // 
            // chkSetPower
            // 
            this.chkSetPower.AutoSize = true;
            this.chkSetPower.Checked = true;
            this.chkSetPower.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSetPower.Location = new System.Drawing.Point(97, 27);
            this.chkSetPower.Name = "chkSetPower";
            this.chkSetPower.Size = new System.Drawing.Size(65, 21);
            this.chkSetPower.TabIndex = 6;
            this.chkSetPower.Text = "Power";
            this.chkSetPower.UseVisualStyleBackColor = true;
            // 
            // chkSetCsc
            // 
            this.chkSetCsc.AutoSize = true;
            this.chkSetCsc.Checked = true;
            this.chkSetCsc.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSetCsc.Location = new System.Drawing.Point(322, 27);
            this.chkSetCsc.Name = "chkSetCsc";
            this.chkSetCsc.Size = new System.Drawing.Size(53, 21);
            this.chkSetCsc.TabIndex = 5;
            this.chkSetCsc.Text = "定速";
            this.chkSetCsc.UseVisualStyleBackColor = true;
            // 
            // dgvDetail
            // 
            this.dgvDetail.AllowUserToAddRows = false;
            this.dgvDetail.AllowUserToDeleteRows = false;
            this.dgvDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDetail.Location = new System.Drawing.Point(12, 56);
            this.dgvDetail.Name = "dgvDetail";
            this.dgvDetail.ReadOnly = true;
            this.dgvDetail.RowHeadersVisible = false;
            this.dgvDetail.RowTemplate.Height = 21;
            this.dgvDetail.Size = new System.Drawing.Size(297, 480);
            this.dgvDetail.TabIndex = 8;
            this.dgvDetail.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvDetail_CellClick);
            // 
            // cboKeyInput
            // 
            this.cboKeyInput.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboKeyInput.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboKeyInput.FormattingEnabled = true;
            this.cboKeyInput.Location = new System.Drawing.Point(84, 47);
            this.cboKeyInput.Name = "cboKeyInput";
            this.cboKeyInput.Size = new System.Drawing.Size(137, 25);
            this.cboKeyInput.TabIndex = 9;
            this.cboKeyInput.SelectedIndexChanged += new System.EventHandler(this.CboKeyInput_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "操作方法";
            // 
            // pnlKey
            // 
            this.pnlKey.Controls.Add(this.btnAtsKey);
            this.pnlKey.Controls.Add(this.chkExclusive);
            this.pnlKey.Controls.Add(this.txtKeyCode);
            this.pnlKey.Controls.Add(this.cboAtsKey);
            this.pnlKey.Controls.Add(this.label4);
            this.pnlKey.Controls.Add(this.label3);
            this.pnlKey.Controls.Add(this.cboKeyInput);
            this.pnlKey.Controls.Add(this.label2);
            this.pnlKey.Controls.Add(this.chkInitialNFB);
            this.pnlKey.Location = new System.Drawing.Point(360, 347);
            this.pnlKey.Name = "pnlKey";
            this.pnlKey.Size = new System.Drawing.Size(400, 156);
            this.pnlKey.TabIndex = 11;
            // 
            // btnAtsKey
            // 
            this.btnAtsKey.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtsKey.Location = new System.Drawing.Point(359, 79);
            this.btnAtsKey.Name = "btnAtsKey";
            this.btnAtsKey.Size = new System.Drawing.Size(26, 28);
            this.btnAtsKey.TabIndex = 13;
            this.btnAtsKey.Text = "?";
            this.toolTip1.SetToolTip(this.btnAtsKey, "実際にキーを入力してキーコードを設定します");
            this.btnAtsKey.UseVisualStyleBackColor = true;
            this.btnAtsKey.Click += new System.EventHandler(this.BtnAtsKey_Click);
            // 
            // chkExclusive
            // 
            this.chkExclusive.AutoSize = true;
            this.chkExclusive.Location = new System.Drawing.Point(6, 123);
            this.chkExclusive.Name = "chkExclusive";
            this.chkExclusive.Size = new System.Drawing.Size(307, 21);
            this.chkExclusive.TabIndex = 14;
            this.chkExclusive.Text = "このDLLをONにした時に他のDLLをOFFにする";
            this.chkExclusive.UseVisualStyleBackColor = true;
            // 
            // txtKeyCode
            // 
            this.txtKeyCode.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtKeyCode.Location = new System.Drawing.Point(280, 79);
            this.txtKeyCode.MaxLength = 5;
            this.txtKeyCode.Name = "txtKeyCode";
            this.txtKeyCode.Size = new System.Drawing.Size(73, 28);
            this.txtKeyCode.TabIndex = 13;
            this.txtKeyCode.Text = "0";
            this.txtKeyCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cboAtsKey
            // 
            this.cboAtsKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAtsKey.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboAtsKey.FormattingEnabled = true;
            this.cboAtsKey.Location = new System.Drawing.Point(91, 82);
            this.cboAtsKey.Name = "cboAtsKey";
            this.cboAtsKey.Size = new System.Drawing.Size(80, 25);
            this.cboAtsKey.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(201, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 17);
            this.label4.TabIndex = 12;
            this.label4.Text = "キーコード";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "ATSキー";
            // 
            // chkNFB
            // 
            this.chkNFB.AutoSize = true;
            this.chkNFB.Checked = true;
            this.chkNFB.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkNFB.Location = new System.Drawing.Point(366, 311);
            this.chkNFB.Name = "chkNFB";
            this.chkNFB.Size = new System.Drawing.Size(138, 21);
            this.chkNFB.TabIndex = 12;
            this.chkNFB.Text = "ON・OFF切替可能";
            this.chkNFB.UseVisualStyleBackColor = true;
            this.chkNFB.CheckedChanged += new System.EventHandler(this.ChkNFB_CheckedChanged);
            // 
            // btnNew
            // 
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNew.Location = new System.Drawing.Point(12, 12);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(60, 28);
            this.btnNew.TabIndex = 13;
            this.btnNew.Text = "新規";
            this.toolTip1.SetToolTip(this.btnNew, "初期化します");
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.BtnNew_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpen.Location = new System.Drawing.Point(78, 12);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(60, 28);
            this.btnOpen.TabIndex = 14;
            this.btnOpen.Text = "開く";
            this.toolTip1.SetToolTip(this.btnOpen, "既存の設定ファイルを開きます");
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.BtnOpen_Click);
            // 
            // btnSave
            // 
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(144, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(60, 28);
            this.btnSave.TabIndex = 15;
            this.btnSave.Text = "保存";
            this.toolTip1.SetToolTip(this.btnSave, "名前をつけて新規保存します");
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnUpd
            // 
            this.btnUpd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpd.Location = new System.Drawing.Point(210, 12);
            this.btnUpd.Name = "btnUpd";
            this.btnUpd.Size = new System.Drawing.Size(60, 28);
            this.btnUpd.TabIndex = 16;
            this.btnUpd.Text = "上書";
            this.toolTip1.SetToolTip(this.btnUpd, "上書き保存します");
            this.btnUpd.UseVisualStyleBackColor = true;
            this.btnUpd.Click += new System.EventHandler(this.BtnUpd_Click);
            // 
            // btnAddRow
            // 
            this.btnAddRow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddRow.Location = new System.Drawing.Point(315, 56);
            this.btnAddRow.Name = "btnAddRow";
            this.btnAddRow.Size = new System.Drawing.Size(26, 28);
            this.btnAddRow.TabIndex = 15;
            this.btnAddRow.Text = "＋";
            this.toolTip1.SetToolTip(this.btnAddRow, "行を追加します");
            this.btnAddRow.UseVisualStyleBackColor = true;
            this.btnAddRow.Click += new System.EventHandler(this.BtnAddRow_Click);
            // 
            // btnRemoveRow
            // 
            this.btnRemoveRow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRemoveRow.Location = new System.Drawing.Point(315, 90);
            this.btnRemoveRow.Name = "btnRemoveRow";
            this.btnRemoveRow.Size = new System.Drawing.Size(26, 28);
            this.btnRemoveRow.TabIndex = 16;
            this.btnRemoveRow.Text = "ー";
            this.toolTip1.SetToolTip(this.btnRemoveRow, "選択している行を削除します");
            this.btnRemoveRow.UseVisualStyleBackColor = true;
            this.btnRemoveRow.Click += new System.EventHandler(this.BtnRemoveRow_Click);
            // 
            // FrmMain
            // 
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.btnAddRow);
            this.Controls.Add(this.btnRemoveRow);
            this.Controls.Add(this.btnUpd);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.chkNFB);
            this.Controls.Add(this.pnlKey);
            this.Controls.Add(this.dgvDetail);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.chkHasSetHandle);
            this.Controls.Add(this.btnSearchFile);
            this.Controls.Add(this.txtFilePath);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("游ゴシック", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Name = "FrmMain";
            this.Text = "PluginConverter";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDetail)).EndInit();
            this.pnlKey.ResumeLayout(false);
            this.pnlKey.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsDetail)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        #region コンストラクタ
        public FrmMain()
        {
            InitializeComponent();
        }
        #endregion

        #region データ
        private void InitData()
        {
            try
            {
                this.CfgFilePath = "";
                this.DispIndex = 0;
                this.CFG = new AtsPlugin.Config();
                this.CFG.DebugMode = true;
                bsDetail.DataSource = this.CFG.Dll;
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "初期化エラー");
            }
        }

        private bool LoadData(string filepath)
        {
            try
            {
                if (this.CFG.Load(filepath) == true)
                {
                    this.CfgFilePath = filepath;
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "ロードエラー");
                return false;
            }
        }


        private void Disp2Data()
        {
            try
            {
                if (this.CFG.Dll.Count >= this.DispIndex)
                {
                    AtsPlugin.Config.CFG_DLL onedll = this.CFG.Dll[this.DispIndex];

                    onedll.OldDllDirectory = HCL.clsHamatetsu.PathEx.GetRelativePath(System.IO.Path.GetDirectoryName(this.CfgFilePath), txtFilePath.Text);
                    onedll.OldDllName = System.IO.Path.GetFileName(onedll.OldDllDirectory);
                    onedll.HasSetHandle = chkHasSetHandle.Checked;
                    onedll.GetHandle = chkGetHandles.Checked;
                    onedll.GetPower = chkGetPower.Checked;
                    onedll.GetBrake = chkGetBrake.Checked;
                    onedll.GetReverser = chkGetReverser.Checked;
                    onedll.SetPower = chkSetPower.Checked;
                    onedll.SetBrake = chkSetBrake.Checked;
                    onedll.SetReverser = chkSetReverser.Checked;
                    onedll.SetCsc = chkSetCsc.Checked;
                    onedll.InitializeNfbOn = chkInitialNFB.Checked;

                    if(onedll.keyControl == null || onedll.keyControl.Count == 0)
                    {
                        onedll.keyControl.Add(new AtsPlugin.Config.CFG_DLL.KeyControl());
                    }

                    if (cboKeyInput.SelectedIndex == (int)AtsPlugin.Config.KeyInput_ENUM.AtsKey)
                    {
                        onedll.keyControl[0].KeyInput = AtsPlugin.Config.KeyInput_ENUM.AtsKey;
                        if (chkNFB.Checked == true)
                        {
                            onedll.keyControl[0].KeyCode = cboAtsKey.SelectedIndex;
                        }
                        else
                        {
                            onedll.keyControl[0].KeyCode = -1;
                        }
                    }
                    else if (cboKeyInput.SelectedIndex == (int)AtsPlugin.Config.KeyInput_ENUM.Keyboard)
                    {
                        onedll.keyControl[0].KeyInput = AtsPlugin.Config.KeyInput_ENUM.Keyboard;
                        if (chkNFB.Checked == true)
                        {
                            onedll.keyControl[0].KeyCode = HCL.clsHamatetsu.ToInt(txtKeyCode.Text);
                        }
                        else
                        {
                            onedll.keyControl[0].KeyCode = -1;
                        }
                    }
                    else
                    {
                        onedll.keyControl[0].KeyInput = AtsPlugin.Config.KeyInput_ENUM.AtsKey;
                        onedll.keyControl[0].KeyCode = -1;
                    }
                    onedll.keyControl[0].IsExclusive = chkExclusive.Checked;

                }

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "データ取り込みエラー");
            }
        }

        private void SaveData(string filepath)
        {
            try
            {
                if(filepath != "" && this.CFG != null)
                {
                    if (this.CFG.Save(filepath) == true)
                    {
                        MessageBox.Show("保存しました");
                    }
                    else
                    {
                        MessageBox.Show("保存に失敗しました");
                    }
                }

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "データ保存エラー");
            }
        }
        #endregion

        #region イベント
        private void FrmMain_Load(object sender, EventArgs e)
        {
            try
            {
                // 
                InitData();

                // 表示初期化
                InitDisp();

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "画面ロードエラー");
                this.Dispose();
            }
        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            try
            {
                InitData();
                DispData();
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "エラー");
            }
        }

        private void BtnOpen_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog OFD = new OpenFileDialog())
                {
                    OFD.Title = "設定ファイルを選択してください";
                    OFD.Filter = "XMLファイル(*.xml)|*.xml|すべてのファイル(*.*)|*.*";
                    OFD.Multiselect = false;

                    if (OFD.ShowDialog() == DialogResult.OK)
                    {
                        if (LoadData(OFD.FileName) == true)
                        {
                            DispData();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "エラー");
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Disp2Data();

                using(SaveFileDialog SFD = new SaveFileDialog())
                {
                    SFD.Filter = "XMLファイル(*.xml)|*.xml";
                    SFD.DefaultExt = ".xml";
                    SFD.Title = "設定ファイルの保存先を指定してください";
                    SFD.OverwritePrompt = true;
                    
                    if (SFD.ShowDialog() == DialogResult.OK)
                    {
                        SaveData(SFD.FileName);
                    }
                }

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "エラー");
            }
        }

        private void BtnUpd_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.CfgFilePath != "")
                {
                    Disp2Data();
                    SaveData(this.CfgFilePath);
                }
                else
                {
                    BtnSave_Click(sender, e);
                }

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "エラー");
            }
        }

        private void DgvDetail_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                // ヘッダークリックは無視
                if(e.RowIndex == -1)
                {
                    return;
                }

                Disp2Data();

                this.DispIndex = e.RowIndex;
                DispData();

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "エラー");
            }
        }

        private void BtnAddRow_Click(object sender, EventArgs e)
        {
            try
            {
                Disp2Data();

                AtsPlugin.Config.CFG_DLL onedll = new AtsPlugin.Config.CFG_DLL();
                onedll.keyControl.Add(new AtsPlugin.Config.CFG_DLL.KeyControl());

                this.CFG.Dll.Add(onedll);

                DispData();

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "行追加エラー");
            }
        }

        private void BtnRemoveRow_Click(object sender, EventArgs e)
        {
            try
            {
                Disp2Data();

                if (this.CFG.Dll.Count == 0)
                {
                    return;
                }
                this.CFG.Dll.RemoveAt(this.DispIndex);
                if (this.CFG.Dll.Count == 0)
                {
                    this.DispIndex = 0;
                }
                else if (this.CFG.Dll.Count <= this.DispIndex)
                {
                    this.DispIndex = this.CFG.Dll.Count - 1;
                    dgvDetail.Rows[this.DispIndex].Selected = true;
                }

                DispData();

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "行削除エラー : " + this.DispIndex.ToString());
            }
        }

        /// <summary>
        /// 参照ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearchFile_Click(object sender, EventArgs e)
        {
            try
            {
                using (OpenFileDialog OFD = new OpenFileDialog())
                {
                    OFD.Title = "プラグインファイルを選択してください";
                    OFD.Filter = "DLLファイル(*.dll)|*.dll";
                    OFD.Multiselect = false;

                    if(OFD.ShowDialog() == DialogResult.OK)
                    {
                        txtFilePath.Text = OFD.FileName;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "ファイル選択エラー");
            }
        }

        /// <summary>
        /// HasSetHandleチェックボックス
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChkHasSetHandle_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (((CheckBox)sender).Checked == false)
                {
                    chkGetHandles.Enabled = false;
                    chkGetHandles.Checked = false;
                    chkGetPower.Enabled = true;
                    chkGetBrake.Enabled = true;
                    chkGetReverser.Enabled = true;
                }
                else
                {
                    chkGetHandles.Enabled = true;
                    chkGetPower.Enabled = false;
                    chkGetPower.Checked = false;
                    chkGetBrake.Enabled = false;
                    chkGetBrake.Checked = false;
                    chkGetReverser.Enabled = false;
                    chkGetReverser.Checked = false;
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "チェックボックスエラー");
            }
        }

        /// <summary>
        /// ONOFF有効
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChkNFB_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                pnlKey.Enabled = ((CheckBox)sender).Checked;
                if(((CheckBox)sender).Checked == false)
                {
                    chkInitialNFB.Checked = true;
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "画面表示エラー");
            }
        }

        /// <summary>
        /// キー入力方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CboKeyInput_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboKeyInput.SelectedIndex <= 0)
                {
                    cboAtsKey.Enabled = true;
                    txtKeyCode.Enabled = false;
                    btnAtsKey.Enabled = false;
                }
                else
                {
                    cboAtsKey.Enabled = false;
                    txtKeyCode.Enabled = true;
                    btnAtsKey.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "画面表示エラー");
            }
        }

        /// <summary>
        /// キーコード画面表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAtsKey_Click(object sender, EventArgs e)
        {
            try
            {
                int c = HCL.clsHamatetsu.ToInt(txtKeyCode.Text);

                using(FrmAtsKey f = new FrmAtsKey())
                {
                    c = f.Main(c);
                }

                if (c > 0)
                {
                    txtKeyCode.Text = c.ToString();
                }
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "画面表示エラー");
            }
        }
        #endregion

        #region 表示
        /// <summary>
        /// 表示初期化
        /// </summary>
        private void InitDisp()
        {
            try
            {
                // コンボボックス作成
                // - キー入力
                cboKeyInput.Items.Clear();
                cboKeyInput.Items.Add("AtsKey");
                cboKeyInput.Items.Add("Keyboard");
                // - ATSキー
                cboAtsKey.Items.Clear();
                cboAtsKey.Items.Add("S");
                cboAtsKey.Items.Add("A1");
                cboAtsKey.Items.Add("A2");
                cboAtsKey.Items.Add("B1");
                cboAtsKey.Items.Add("B2");
                cboAtsKey.Items.Add("C1");
                cboAtsKey.Items.Add("C2");
                cboAtsKey.Items.Add("D");
                cboAtsKey.Items.Add("E");
                cboAtsKey.Items.Add("F");
                cboAtsKey.Items.Add("G");
                cboAtsKey.Items.Add("H");
                cboAtsKey.Items.Add("I");
                cboAtsKey.Items.Add("J");
                cboAtsKey.Items.Add("K");
                cboAtsKey.Items.Add("L");

                // グリッドビュー
                InitDataGridViewColumn();

                dgvDetail.DataSource = bsDetail;
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "画面初期化エラー");
            }
        }

        /// <summary>
        /// グリッドビューのカラムの設定
        /// </summary>
        private void InitDataGridViewColumn()
        {

            DataGridViewColumn[] cols = new DataGridViewColumn[1];
            int i = 0;

            try
            {
                dgvDetail.Columns.Clear();

                //自動的にカラムを作らない様にする
                dgvDetail.AutoGenerateColumns = false;

                // 0カラム目(ファイル名)
                cols[i] = new DataGridViewTextBoxColumn();
                cols[i].DataPropertyName = "OldDllName";
                cols[i].HeaderText = "DLLパス";
                cols[i].Name = "OldDllName";
                cols[i].ReadOnly = true;
                cols[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

                i += 1;

                foreach (DataGridViewColumn col in cols)
                {
                    // 共通の設定はここでセット
                    col.SortMode = DataGridViewColumnSortMode.NotSortable;
                    col.Resizable = DataGridViewTriState.False;

                    dgvDetail.Columns.Add(col);
                }

            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "グリッドビュー定義エラー");
            }

        }

        /// <summary>
        /// 画面表示
        /// </summary>
        private void DispData()
        {
            try
            {
                if (CFG != null && CFG.Dll != null && this.CFG.Dll.Count > 0)
                {
                    AtsPlugin.Config.CFG_DLL onedll = this.CFG.Dll[this.DispIndex];

                    txtFilePath.Text = HCL.clsHamatetsu.PathEx.GetAbsolutePath(this.CfgFilePath, onedll.OldDllDirectory);
                    chkHasSetHandle.Checked = onedll.HasSetHandle;
                    if (onedll.HasSetHandle == true)
                    {
                        chkGetHandles.Checked = onedll.GetHandle;
                        chkGetPower.Checked = true;
                        chkGetBrake.Checked = true;
                        chkGetReverser.Checked = true;
                    }
                    else
                    {
                        chkGetHandles.Checked = false;
                        chkGetPower.Checked = onedll.GetPower;
                        chkGetBrake.Checked = onedll.GetBrake;
                        chkGetReverser.Checked = onedll.GetReverser;
                    }
                    chkSetPower.Checked = onedll.SetPower;
                    chkSetBrake.Checked = onedll.SetBrake;
                    chkSetReverser.Checked = onedll.SetReverser;
                    chkSetCsc.Checked = onedll.SetCsc;
                    chkInitialNFB.Checked = onedll.InitializeNfbOn;
                    if (onedll.keyControl != null && onedll.keyControl.Count > 0)
                    {
                        if ((onedll.keyControl[0].KeyInput == AtsPlugin.Config.KeyInput_ENUM.AtsKey && onedll.keyControl[0].KeyCode <= -1) ||
                            (onedll.keyControl[0].KeyInput != AtsPlugin.Config.KeyInput_ENUM.AtsKey && onedll.keyControl[0].KeyCode == 0))
                        {
                            chkNFB.Checked = false;
                        }
                        else
                        {
                            chkNFB.Checked = true;
                        }
                        cboKeyInput.Text = onedll.keyControl[0].KeyInput.ToString();
                        if (onedll.keyControl[0].KeyInput == AtsPlugin.Config.KeyInput_ENUM.AtsKey)
                        {
                            cboAtsKey.SelectedIndex = onedll.keyControl[0].KeyCode;
                            txtKeyCode.Text = "";
                        }
                        else
                        {
                            cboAtsKey.SelectedIndex = 0;
                            txtKeyCode.Text = onedll.keyControl[0].KeyCode.ToString();
                        }
                        chkExclusive.Checked = onedll.keyControl[0].IsExclusive;
                    }
                    else
                    {
                        chkNFB.Checked = false;
                        cboKeyInput.Text = "AtsKey";
                        cboAtsKey.SelectedIndex = 0;
                        txtKeyCode.Text = "";
                        chkExclusive.Checked = false;
                    }

                }
                else
                {
                    txtFilePath.Text = "";
                    chkHasSetHandle.Checked = false;
                    chkGetHandles.Checked = false;
                    chkGetPower.Checked = true;
                    chkGetBrake.Checked = true;
                    chkGetReverser.Checked = true;
                    chkSetPower.Checked = true;
                    chkSetBrake.Checked = true;
                    chkSetReverser.Checked = true;
                    chkSetCsc.Checked = false;
                    chkNFB.Checked = false;
                    chkInitialNFB.Checked = true;
                    cboKeyInput.Text = "AtsKey";
                    txtKeyCode.Text = "";
                    chkExclusive.Checked = false;

                }

                bsDetail.ResetBindings(false);
            }
            catch (Exception ex)
            {
                ExceptionMessage(ex, "画面表示エラー");
            }
        }
        #endregion


        #region utility
        /// <summary>
        /// 例外発生ダイアログ
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="caption"></param>
        /// <param name="extraStr"></param>
        private void ExceptionMessage(Exception ex, string caption, string extraStr = "")
        {
            try
            {
                MessageBox.Show(extraStr + "\n" + ex.Message, caption);
            }
            catch (Exception ex2)
            {
                MessageBox.Show(ex2.Message);
            }
        }
        #endregion

    }
}
