﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtsPlugin
{
    public class EngineUnique : EngineBase
    {
        /*
[3] PT閉塞地上子
    保持している地上子距離程と異なった場合はリセット
    信号インデックスが0かつ保持しているOptionalより小さな値が入ってきた場合は保持するOptinalとDistanceを更新
    Optionalが9以上の場合は信号インデックスが0であるとみなす
    保持しているデータをPTプラグインに渡す
    [PT60]に変換して渡す
[4][5] PT即時停止地上子
    信号インデックスが0なら即時停止
    5は無閉塞を許可する
    [PT60]に変換して渡す
[6][9][10] PT速度制限
[8] PT下り勾配速度制限
    Optionalの下３桁が制限速度、上が制限区間までの距離
    [PT70]に変換して渡す
[16][18][19][20] PT速度制限区間終了
    [PT70]に制限開始距離4・制限区間長4として渡す
[25] PE地上子
    0:[PT90]にそのまま渡す
    1:[PT68]
    2:[PT69]
[0] STロング
    [PT0]にそのまま渡す
[1] ST直下
    [PT1]にそのまま渡す
[55] ST速度照査
    Optionalに指定速度
    上回っていた場合は即時停止
    [PT10]に速度指定で渡す
[56] ST速度照査
    Optionalに指定速度
    上回っていた場合は即時停止
    [PT10]に速度指定で渡す
[203]勾配(‰)
    */

        private static double pBeaconDistance = 0;
        private static int pBeaconCount = 0;
        private static float pSectionDistance = 0;
        private static float[] LimitDistance = new float[4];
        private static int[] LimitSpeed = new int[4];

        public static int GetLimitSpeed(int index)
        {
            if (0 <= index && index <= 3)
            {
                return LimitSpeed[index];
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// 地上子通過イベント
        /// </summary>
        /// <param name="beaconData"></param>
        /// <returns></returns>
        public static AtsDefine.AtsBeaconData SetBeaconData(AtsDefine.AtsBeaconData beaconData, AtsEngine.MyVehicleState pMyVeshicleState)
        {
            AtsDefine.AtsBeaconData nBeaconData;
            nBeaconData = beaconData;

            try
            {
                switch (beaconData.Type)
                {
                    case 0:
                        // ATS-S ロング
                        nBeaconData.Type = 0;
                        break;

                    case 1:
                        // ATS-S 即時停止
                        nBeaconData.Type = 0;
                        break;

                    case 3: // PT TL・TR地上子
                        if (beaconData.Optional < 9)
                        {
                            if (pBeaconDistance != pMyVeshicleState.Location)
                            {
                                pBeaconDistance = pMyVeshicleState.Location;
                                pBeaconCount = -1;
                                pSectionDistance = 0;
                            }
                            if (pBeaconCount < 0 || (pBeaconCount < beaconData.Optional && ((beaconData.Distance < pSectionDistance) || (pSectionDistance == 0))))
                            {
                                if (beaconData.Signal == 0)
                                {
                                    pBeaconCount = 10;
                                    pSectionDistance = beaconData.Distance - 10;

                                    nBeaconData.Type = 60;
                                    nBeaconData.Signal = 4;
                                    nBeaconData.Distance = (float)(pSectionDistance);
                                    if (pSectionDistance > 3996)
                                    {
                                        nBeaconData.Optional = 1999020020; // 無閉塞有効
                                    }
                                    else
                                    {
                                        nBeaconData.Optional = (int)(pSectionDistance / 4) * 1000000 + 1000000000 + 20020; // 無閉塞有効
                                    }
                                }
                            }
                        }
                        else
                        {
                            if ((pBeaconDistance != pMyVeshicleState.Location) || (pBeaconCount < 0))
                            {
                                pBeaconCount = 9;
                                pSectionDistance = beaconData.Distance - 10;
                            }
                            //Optional = 0 xxx 020 020の形に変換して60番地上子に送る
                            if (pSectionDistance <= 0) pSectionDistance = 4;
                            if (pSectionDistance > 4000) pSectionDistance = 3999;
                            nBeaconData.Type = 60;
                            nBeaconData.Signal = 4;
                            nBeaconData.Distance = (float)(pSectionDistance);
                            if (pSectionDistance > 3996)
                            {
                                nBeaconData.Optional = 1999020020; // 無閉塞有効
                            }
                            else
                            {
                                nBeaconData.Optional = (int)(pSectionDistance / 4) * 1000000 + 1000000000 + 20020; // 無閉塞有効
                            }
                        }
                        break;

                    case 4: // PT TS地上子 (即停)
                        if (beaconData.Signal == 0 && beaconData.Distance < 50)
                        {
                            nBeaconData.Type = 60;
                            nBeaconData.Signal = 0;
                            nBeaconData.Distance = beaconData.Distance;
                            nBeaconData.Optional = (int)(beaconData.Distance / 4) * 1000000 + 20020; // 即時停止有効
                        }
                        break;

                    case 5: // PT TM地上子
                        if (beaconData.Signal == 0 && beaconData.Distance < 50)
                        {
                            nBeaconData.Type = 60;
                            nBeaconData.Signal = 0;
                            nBeaconData.Distance = beaconData.Distance;
                            nBeaconData.Optional = (int)(beaconData.Distance / 4) * 1000000 + 1000000000 + 20020; // 無閉塞有効
                        }
                        break;

                    case 6: // PT 速度制限
                        LimitDistance[0] = (float)((int)(beaconData.Optional / 1000));
                        LimitSpeed[0] = beaconData.Optional % 1000;
                        SetLimitBeacon(beaconData);
                        break;
                    case 8: // PT 下り勾配速度制限
                        LimitDistance[1] = (float)((int)(beaconData.Optional / 1000));
                        LimitSpeed[1] = beaconData.Optional % 1000;
                        SetLimitBeacon(beaconData);
                        break;
                    case 9: // PT 分岐器速度制限
                        LimitDistance[2] = (float)((int)(beaconData.Optional / 1000));
                        LimitSpeed[2] = beaconData.Optional % 1000;
                        SetLimitBeacon(beaconData);
                        break;
                    case 10: // PT 臨時速度制限
                        LimitDistance[3] = (float)((int)(beaconData.Optional / 1000));
                        LimitSpeed[3] = beaconData.Optional % 1000;
                        SetLimitBeacon(beaconData);
                        /*
                                                beaconData.Type = 70;
                                                beaconData.Signal = 4;
                                                beaconData.Distance = sectionDistance;
                                                beaconData.Optional = (int)((beaconData.Optional % 1000) / 5) * 1000000 + (int)(beaconData.Optional / 4000) * 1000 + 999;
                                                PTSetBeaconData(beaconData);
                        */
                        break;
                    case 16: // PT 速度制限解除
                        if (LimitSpeed[0] < 999)
                        {
                            // いったん制限解除
                            //nBeaconData.Type = 313019;
                            //dllstruct.setBeaconDataDelegate(nBeaconData);

                            LimitDistance[0] = 0;
                            LimitSpeed[0] = 999;
                            nBeaconData = SetLimitBeacon(beaconData);
                        }
                        break;
                    case 18: // PT 下り勾配速度制限解除
                        if (LimitSpeed[1] < 999)
                        {
                            // いったん制限解除
                            //nBeaconData.Type = 313019;
                            //dllstruct.setBeaconDataDelegate(nBeaconData);

                            LimitDistance[1] = 0;
                            LimitSpeed[1] = 999;
                            nBeaconData = SetLimitBeacon(beaconData);
                        }
                        break;
                    case 19: // PT 分岐器速度制限解除
                        if (LimitSpeed[2] < 999)
                        {
                            // いったん制限解除
                            //nBeaconData.Type = 313019;
                            //dllstruct.setBeaconDataDelegate(nBeaconData);

                            LimitDistance[2] = 0;
                            LimitSpeed[2] = 999;
                            nBeaconData = SetLimitBeacon(beaconData);
                        }
                        break;
                    case 20: // PT 臨時速度制限解除
                        if (LimitSpeed[3] < 999)
                        {
                            // いったん制限解除
                            //nBeaconData.Type = 313019;
                            //dllstruct.setBeaconDataDelegate(nBeaconData);

                            LimitDistance[3] = 0;
                            LimitSpeed[3] = 999;
                            nBeaconData = SetLimitBeacon(beaconData);
                        }
                        /*
                                                beaconData.Type = 70;
                                                beaconData.Signal = 4;
                                                beaconData.Distance = sectionDistance;
                                                beaconData.Optional = 999000001 + ((g_cars * 5) % 1000) * 1000; // 1両20m換算
                                                PTSetBeaconData(beaconData);
                        */
                        break;

                    case 25: // PE地上子
                        if (beaconData.Optional == 0)
                        {
                            // PE地上子
                            nBeaconData.Type = 90;
                        }
                        else if (beaconData.Optional == 1)
                        {
                            // KE地上子 (拠点PでPを復活)
                            //beaconData.Type = 69;
                            //PTSetBeaconData(beaconData);
                        }
                        else if (beaconData.Optional == 2)
                        {
                            // PE地上子 (拠点PでPを休止)
                            nBeaconData.Type = 90;
                        }
                        break;

                    case 55: // ST速度制限
                    case 56: // ST速度制限
                        if (pMyVeshicleState.NewState.Speed > 0)
                        {
                            nBeaconData.Type = 313010;
                            if (beaconData.Optional > 0)
                            {
                                nBeaconData.Optional = beaconData.Optional * 10;
                            }
                            else
                            {
                                // 強制停止
                                nBeaconData.Optional = -1;
                            }
                        }
                        break;

                    case 203:
                        // 勾配設定
                        nBeaconData.Type = 75;
                        break;

                    case 313019:
                        // (再帰用)制限解除
                        nBeaconData.Type = 313019;
                        break;

                    default:
                        nBeaconData.Type = -1;
                        break;
                }
            }
            catch (Exception ex)
            {
                Log.Write("[BeaconUq] (Error) : " + ex.Message);
                nBeaconData.Type = -1;
            }
            return nBeaconData;
        }

        /// <summary>
        /// 制限速度系地上子通過処理
        /// </summary>
        /// <param name="beaconData"></param>
        /// <returns></returns>
        private static AtsDefine.AtsBeaconData SetLimitBeacon(AtsDefine.AtsBeaconData beaconData)
        {
            AtsDefine.AtsBeaconData pBeaconData = beaconData;
            pBeaconData.Type = -1;

            int LS = 999;
            float LD = 0;
            int i = 0;
            for (i = 0; i < 4; i++)
            {
                if (LS > LimitSpeed[i] && LimitDistance[i] > 0)
                {
                    LS = LimitSpeed[i];
                    LD = LimitDistance[i];
                    if (LD >= 4000)
                    {
                        LD = 3999;
                    }
                }
            }

            // いったん制限解除
            //this.AtsPT.ResetLimitPattern();

            if (LS < 999)
            {
                // 制限
                pBeaconData.Type = 70;
                pBeaconData.Signal = 4;
                pBeaconData.Distance = (float)(pSectionDistance);
                pBeaconData.Optional = (int)(LS / 5) * 1000000 + (int)(LD / 4) * 1000 + 1999;
            }

            return pBeaconData;
        }

    }
}
