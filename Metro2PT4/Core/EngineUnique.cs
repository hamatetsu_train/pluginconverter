﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtsPlugin
{
    public class EngineUnique : EngineBase
    {
        /* メトロ総合プラグイン→ATS-PTプラグインv4
         * Signal
         * <= 9  0 (-1)
         *   10  1 (0)
         * <=15  2 (25)
         * <=18  3 (40)
         * <=21  4 (55)
         * <=23  5 (65)
         * <=25  6 (75)
         *   26  8 (80)
         *   27  9 (85)
         *   28 10 (90)
         *   29 11 (95)
         *   30 12 (100)
         *   31 13 (105)
         *   32 14 (110)
         *   33 16 (120)
         * <=36  0 (-1)
         * >=37 40 (-2)
         * 
         * Beacon
         *   7 -> 7
         *  12 -> 12
         */

        /// <summary>
        /// 信号変化イベント
        /// </summary>
        /// <param name="signalIndex"></param>
        /// <returns></returns>
        public static int SetSignal(int signalIndex)
        {
            try
            {
                if (0 <= signalIndex && signalIndex <= 9)
                {
                    return 0;
                }
                else if (signalIndex == 10)
                {
                    return 1;
                }
                else if (signalIndex <= 15)
                {
                    return 2;
                }
                else if (signalIndex <= 18)
                {
                    return 3;
                }
                else if (signalIndex <= 21)
                {
                    return 4;
                }
                else if (signalIndex <= 23)
                {
                    return 5;
                }
                else if (signalIndex <= 25)
                {
                    return 6;
                }
                else if (26 <= signalIndex && signalIndex <= 33)
                {
                    return (signalIndex - 26) + 8;
                }
                else if (signalIndex <= 38)
                {
                    return -1;
                }
                else if (signalIndex == 39)
                {
                    return 44;
                }
                else if (signalIndex <= 42)
                {
                    return 43;
                }
                else if (signalIndex <= 47)
                {
                    return 42;
                }
                else if (signalIndex == 48)
                {
                    return 41;
                }
                else if (signalIndex == 49)
                {
                    return 40;
                }
                else if (signalIndex == 50)
                {
                    return 1;
                }
                else if (signalIndex == 51)
                {
                    return 2;
                }
                else if (signalIndex == 52)
                {
                    return 3;
                }
                else if (signalIndex == 53)
                {
                    return 5;
                }
                else if (signalIndex == 55)
                {
                    return 19;
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception ex)
            {
                Log.Error("[SignalUq] : " + ex.Message,ex);
                return -1;
            }
        }

        /// <summary>
        /// 地上子通過イベント
        /// </summary>
        /// <param name="beaconData"></param>
        /// <returns></returns>
        public static AtsDefine.AtsBeaconData SetBeaconData(AtsDefine.AtsBeaconData beaconData)
        {
            AtsDefine.AtsBeaconData nBeaconData;
            nBeaconData = beaconData;

            try
            {
                if (beaconData.Type == 7 || beaconData.Type == 12 || beaconData.Type == 31)
                {
                    // 7:前方予告 12:ORP 31:前方予告
                    // そのまま通す
                }
                else
                {
                    nBeaconData.Type = -1;
                }
            }
            catch (Exception ex)
            {
                Log.Error("[BeaconUq] : " + ex.Message, ex);
                nBeaconData.Type = -1;
            }
            return nBeaconData;
        }


    }
}
